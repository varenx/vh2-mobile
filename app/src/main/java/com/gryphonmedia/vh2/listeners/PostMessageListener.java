package com.gryphonmedia.vh2.listeners;

public interface PostMessageListener {
    void onPostMessage(String postMessage, String messageTo, String eyeconId);
}
