package com.gryphonmedia.vh2.listeners;

import com.gryphonmedia.vh2.models.FavColour;

public interface EyeconListener {
    void onEyeconChange(FavColour eyecon);
}
