package com.gryphonmedia.vh2.listeners;

public interface LoginListener {
    void onLogin(String username, String password);
    void onCharacterLogin(int roomId, String charName);
}
