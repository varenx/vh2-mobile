package com.gryphonmedia.vh2.listeners;

public interface StatusListener {
    void onStatusChange(String code);
}
