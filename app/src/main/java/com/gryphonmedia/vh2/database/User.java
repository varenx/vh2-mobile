package com.gryphonmedia.vh2.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class User {
    @PrimaryKey(autoGenerate = true)
    private int sessionId;
    private int charId;
    private String eyecon;
    private String name;
    private String colour;
    private String icon;
    private String gender;
    private String status;

    public User(int sessionId, int charId, String eyecon, String name, String colour, String icon, String gender, String status) {
        this.sessionId = sessionId;
        this.charId = charId;
        this.eyecon = eyecon;
        this.name = name;
        this.colour = colour;
        this.icon = icon;
        this.gender = gender;
        this.status = status;
    }

    public int getSessionId() {
        return sessionId;
    }

    public void setSessionId(int sessionId) {
        this.sessionId = sessionId;
    }

    public int getCharId() {
        return charId;
    }

    public void setCharId(int charId) {
        this.charId = charId;
    }

    public String getEyecon() {
        return eyecon;
    }

    public void setEyecon(String eyecon) {
        this.eyecon = eyecon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getGender() {
        return gender.toLowerCase();
    }

    public void setGender(String gender) {
        this.gender = gender.toLowerCase();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
        if (this.status == "") {
            this.status = "online";
        }
    }

    public String toString() {
        return this.colour + " " + this.name + " [" + this.sessionId + ", " + this.charId + "]";
    }
}
