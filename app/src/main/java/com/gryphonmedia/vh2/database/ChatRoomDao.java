package com.gryphonmedia.vh2.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface ChatRoomDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertChatRoom(ChatRoom chatRoom);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<ChatRoom> chatRooms);

    @Delete
    void deleteChatRoom(ChatRoom chatRoom);

    @Query("SELECT * FROM chatrooms WHERE id = :id")
    ChatRoom findById(int id);

    @Query("SELECT * FROM chatrooms ORDER BY name ASC")
    LiveData<List<ChatRoom>> findAll();

    @Query("DELETE FROM chatrooms")
    int deleteAll();

    @Query("SELECT COUNT(*) FROM chatrooms")
    int getCount();
}
