package com.gryphonmedia.vh2.database;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

@androidx.room.Database(entities = {
        Character.class,
        ChatRoom.class,
        Post.class,
        User.class,
        SmallIcon.class,
}, version = 4, exportSchema = false)
public abstract class Database extends RoomDatabase {
    public static final String DATABASE_NAME = "vh2.db";
    private static volatile Database instance;
    private static final Object LOCK = new Object();

    public abstract CharacterDao characterDao();
    public abstract ChatRoomDao chatRoomDao();
    public abstract PostDao postDao();
    public abstract UserDao userDao();
    public abstract SmallIconDao smallIconDao();

    public static Database getInstance(Context context) {
        if (instance == null) {
            synchronized (LOCK) {
                if (instance == null) {
                    if (context != null) {
                        instance = Room.databaseBuilder(context.getApplicationContext(),
                                Database.class, DATABASE_NAME)
                                .fallbackToDestructiveMigration()
                                .build();
                    }
                }
            }
        }
        return instance;
    }
}
