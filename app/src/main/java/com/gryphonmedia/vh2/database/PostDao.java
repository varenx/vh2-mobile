package com.gryphonmedia.vh2.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface PostDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertPost(Post post);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Post> posts);

    @Delete
    void deletePost(Post post);

    @Query("SELECT * FROM posts WHERE id = :id")
    Post findById(long id);

    @Query("SELECT * FROM posts ORDER BY id ASC")
    LiveData<List<Post>> findAll();

    @Query("DELETE FROM posts")
    int deleteAll();

    @Query("SELECT COUNT(*) FROM posts")
    int getCount();
}
