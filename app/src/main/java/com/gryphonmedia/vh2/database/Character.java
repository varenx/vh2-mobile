package com.gryphonmedia.vh2.database;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "characters")
public class Character {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private String name;

    @Ignore
    public Character() {
    }

    public Character(int id, String name)
    {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString()
    {
        return id + " " + name;
    }

}
