package com.gryphonmedia.vh2.database;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.gryphonmedia.vh2.utils.StringProcessor;

@Entity(tableName = "posts")
public class Post {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String text;
    private String sender;
    private String senderColour;
    private String recipient;
    private String recipientColour;
    private String textColour;
    private String eyecon;
    private String msgTime;
    private String msgDate;
    private boolean isPose;
    private boolean isPrivate;

    public Post(long id, String text, String sender, String senderColour, String recipient, String recipientColour, String textColour, String eyecon, String msgTime, String msgDate, boolean isPose, boolean isPrivate) {
        this.id = id;
        this.text = StringProcessor.removeHtml(
                StringProcessor.stripLinks(text.trim())
        );
        this.sender = sender;
        this.senderColour = senderColour;
        this.recipient = recipient;
        this.recipientColour = recipientColour;
        this.textColour = textColour;
        this.eyecon = eyecon;
        this.msgTime = msgTime;
        this.msgDate = msgDate;
        this.isPose = isPose;
        this.isPrivate = isPrivate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderColour() {
        String result = senderColour
                .replace("color: ", "")
                .replace("<span  style='", "")
                .replace("<span style='", "")
                .replace(";'>", "")
                .replace(";", "")
                .replace("</span>", "")
                .replace("font-style: italic", "")
                .replace(this.sender, "");
        if (result.equals("")) { result = "#FFFFFF"; }
        return result;
    }

    public void setSenderColour(String senderColour) {
        this.senderColour = senderColour;
        this.senderColour = this.senderColour.replace("<span  style='", "");
        this.senderColour = this.senderColour.replace("'>", "");
        this.senderColour = this.senderColour.replace("</span>", "");
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipientColour() {
        return recipientColour;
    }

    public void setRecipientColour(String recipientColour) {
        this.recipientColour = recipientColour;
    }

    public String getTextColour() {
        return textColour;
    }

    public void setTextColour(String textColour) {
        this.textColour = textColour;
        this.textColour = this.textColour.replace("<span  style='color: ", "");
        this.textColour = this.textColour.replace("'>", "");
        this.textColour = this.textColour.replace("<\\/span>", "");
    }

    public String getEyecon() {
        return eyecon;
    }

    public void setEyecon(String eyecon) {
        this.eyecon = eyecon;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public boolean isPose() {
        return isPose;
    }

    public void setPose(boolean pose) {
        isPose = pose;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public String poseString() {
        String res = (isPose) ? this.sender + " " + this.text : this.text;
        res = res.replaceAll("\\s\\s+", " ");
        return res.trim();
    }

    public String toString() {
        if (this.isPose) {
            return "[" + this.msgTime + "] " + this.sender + this.text;
        } else {
            return "[" + this.msgTime + "] " + this.sender + ": " + this.text;
        }
    }
}
