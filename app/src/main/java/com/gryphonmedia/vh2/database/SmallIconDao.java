package com.gryphonmedia.vh2.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface SmallIconDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIcon(SmallIcon icon);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<SmallIcon> icons);

    @Delete
    void deleteIcon(SmallIcon icon);

    @Query("SELECT * FROM smallicons ORDER BY name ASC")
    LiveData<List<SmallIcon>> findAll();

    @Query("SELECT * FROM smallicons WHERE name = :name")
    SmallIcon findByName(String name);
}
