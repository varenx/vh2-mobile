package com.gryphonmedia.vh2.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface CharacterDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertCharacter(Character character);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<Character> characters);

    @Delete
    void deleteCharacter(Character character);

    @Query("SELECT * FROM characters WHERE id = :id")
    Character findById(int id);

    @Query("SELECT * FROM characters ORDER BY name ASC")
    LiveData<List<Character>> findAll();

    @Query("DELETE FROM characters")
    int deleteAll();

    @Query("SELECT COUNT(*) FROM characters")
    int getCount();
}
