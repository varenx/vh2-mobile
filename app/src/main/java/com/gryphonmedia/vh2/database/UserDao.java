package com.gryphonmedia.vh2.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertUser(User user);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<User> users);

    @Delete
    void deleteUser(User user);

    @Query("SELECT * FROM users WHERE sessionId = :id")
    User findById(int id);

    @Query("SELECT * FROM users WHERE charId = :charId")
    User findByCharId(int charId);

    @Query("SELECT * FROM users WHERE name = :name")
    User findByName(String name);

    @Query("SELECT * FROM users ORDER BY sessionId ASC")
    LiveData<List<User>> findAll();

    @Query("UPDATE users SET eyecon = :eyecon WHERE name = :name")
    void updateEyeconByName(String name, String eyecon);

    @Query("DELETE FROM users")
    int deleteAll();

    @Query("SELECT COUNT(*) FROM users")
    int getCount();
}
