package com.gryphonmedia.vh2.database;

import android.content.Context;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.gryphonmedia.vh2.utils.Constants;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class Repository {

    private static final String TAG = Repository.class.getSimpleName();

    private static Repository ourInstance;

    public LiveData<List<Character>> mCharacters;
    public LiveData<List<ChatRoom>> mRooms;
    public LiveData<List<Post>> mPosts;
    public LiveData<List<User>> mUsers;
    public LiveData<List<SmallIcon>> mSmallIcons;
    private Database mDb;
    private Executor executor = Executors.newSingleThreadExecutor();

    public static Repository getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new Repository(context);
        }
        return ourInstance;
    }

    private Repository(Context context) {
        mDb = Database.getInstance(context);
        mCharacters = getAllCharacters();
        mRooms = getAllChatRooms();
        mPosts = getAllPosts();
        mUsers = getAllUsers();
        mSmallIcons = getAllSmallIcons();
    }

    private LiveData<List<Character>> getAllCharacters()
    {
        try {
            return mDb.characterDao().findAll();
        } catch (NullPointerException e) {

        }
        return null;
    }

    private LiveData<List<ChatRoom>> getAllChatRooms() {
        try {
            return mDb.chatRoomDao().findAll();
        } catch (NullPointerException e) {}
        return null;
    }

    public LiveData<List<Post>> getAllPosts() { return mDb.postDao().findAll(); }

    public LiveData<List<User>> getAllUsers() { return mDb.userDao().findAll(); }

    public LiveData<List<SmallIcon>> getAllSmallIcons() {
        return mDb.smallIconDao().findAll();
    }

    public void insertCharacter(final Character character) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.characterDao().insertCharacter(character);
            }
        });
    }

    public void insertChatRoom(final ChatRoom chatRoom) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.chatRoomDao().insertChatRoom(chatRoom);
            }
        });
    }

    public void insertAllIcons(final List<SmallIcon> icons) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.smallIconDao().insertAll(icons);
            }
        });
    }

    public void wipeRoomData() {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.postDao().deleteAll();
                mDb.userDao().deleteAll();
            }
        });
    }

    public void insertPost(final Post post) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.postDao().insertPost(post);
            }
        });
    }

    public void deletePostById(final long id) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                Log.i(TAG, "Trying to delete post " + id);
                Post post = mDb.postDao().findById(id);
                if (post != null) {
                    Log.i(TAG, "Post " + post.getId() + " found, text: \"" + post.poseString() + "\"");
                    mDb.postDao().deletePost(post);
                } else {
                    Log.i(TAG, "Post not found?");
                }
            }
        });
    }

    public void insertUser(final User user) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.userDao().insertUser(user);
            }
        });
    }

    public User findUser(int charId) { return mDb.userDao().findByCharId(charId); }

    public User findUser(String name) { return mDb.userDao().findByName(name); }

    public String findIconByUserName(String name) {
        SmallIcon icon = mDb.smallIconDao().findByName(name);
        if (icon == null) {
            return Constants.DEFAULT_AVATAR;
        }
        return icon.getImage();
    }

    public void updateUserIconByName(final String name, final String eyecon) {
        executor.execute(new Runnable() {
            @Override
            public void run() {
                mDb.userDao().updateEyeconByName(name, eyecon);
            }
        });
    }

    public void deleteUserBySessionId(int sessionId) {
        User user = mDb.userDao().findById(sessionId);
        if (user != null) {
            mDb.userDao().deleteUser(user);
        }
    }
}
