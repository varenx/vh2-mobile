package com.gryphonmedia.vh2;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gryphonmedia.vh2.database.Post;
import com.gryphonmedia.vh2.database.Repository;
import com.gryphonmedia.vh2.database.SmallIcon;
import com.gryphonmedia.vh2.database.User;
import com.gryphonmedia.vh2.models.ChatEvent;
import com.gryphonmedia.vh2.models.chatevents.MessageDelete;
import com.gryphonmedia.vh2.models.chatevents.RoomChat;
import com.gryphonmedia.vh2.models.chatevents.UserlistRemove;
import com.gryphonmedia.vh2.models.chatevents.UserlistUpdate;
import com.gryphonmedia.vh2.models.chatevents.update.Add;
import com.gryphonmedia.vh2.services.SetStatusService;
import com.gryphonmedia.vh2.ui.Notifications;
import com.gryphonmedia.vh2.utils.DingPhrases;
import com.gryphonmedia.vh2.utils.HighlightedUsers;
import com.gryphonmedia.vh2.utils.IgnoredUsers;
import com.gryphonmedia.vh2.utils.StringProcessor;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.List;

public class ChatEventHandler {

    private static final String TAG = ChatEventHandler.class.getSimpleName();
    private static final String SETTING_YOUR_STATUS_IDLE = "You have been idle for too long, setting your status.";

    private Repository mRepository;
    private Context mContext;
    private String mCharName;
    private String mUsername;
    private String currentEyecon;

    public ChatEventHandler(Context context) {
        mRepository = Repository.getInstance(mContext);
        mContext = context;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        mCharName = prefs.getString("current_character", "");
        mUsername = prefs.getString("username", "");
        currentEyecon = prefs.getString("current_eyecon", "");
    }

    private void eventRoomChat(RoomChat msg) {
        boolean notified = false;
        if (IgnoredUsers.isUserIgnored(mContext, msg.getFrom().getName())) {
            // The user is ignored, so ignore them!
            return;
        }
        if (mUsername.equals("Varenx")) {
            // Log.i(TAG, "Run code specific to Varenx");
            Log.i(TAG, "++ \"" + msg.getFrom().getName() + "\": \"" + msg.getBody() + "\"");
            if (msg.getBody().equals(SETTING_YOUR_STATUS_IDLE)) {
                if (!msg.getFrom().getName().equals("System")) {
                    return;
                }
                Log.i(TAG, "We got a system notification about our idling, that won't do!");
                // And we should set our status back to online
                Intent intent = new Intent(mContext, SetStatusService.class);
                intent.putExtra("status", "online");
                mContext.startService(intent);

                return;
            }
        }
        Post post = new Post(
                msg.getId(),
                msg.getBody(),
                msg.getFrom().getName(),
                msg.getFrom().getPrettyName(),
                msg.getTo().getName(),
                msg.getTo().getPrettyName(),
                msg.getTextColor(),
                msg.getEyecon(),
                msg.getMsgTime(),
                msg.getMsgDate(),
                msg.isPose(),
                msg.isPrivate()
        );
        // Log.i(TAG, post.toString());
        mRepository.insertPost(post);
        if (msg.getTo().getName().equals(mCharName) && msg.isPrivate()) {
            if (!msg.getFrom().getName().equals(mCharName)) {
                // Display a notification
                String title = "Private message to " + mCharName;
                String text = msg.getFrom().getName() + " sent you a whisper: " + post.poseString();
                chatNotification(title, text);
                notified = true;
            }
        } else if (StringProcessor.containsInsensitive(msg.getBody(), mCharName)) {
            // Display a notification
            if (!msg.getFrom().getName().equals(mCharName)) {
                String title = mCharName + " mentioned";
                String text = msg.getFrom().getName() + " mentioned you in their post: " + post.poseString();
                chatNotification(title, text);
                notified = true;
            }
        } else if (HighlightedUsers.isNameHighlighted(mContext, msg.getFrom().getName())) {
            if (!msg.getFrom().getName().equals(mCharName)) {
                String title = msg.getFrom().getName() + " posted in the chat";
                String text = msg.getFrom().getName() + " posted in the chat: " + post.poseString();
                chatNotification(title, text);

                notified = true;
            }
        }
        if (!notified) {
            String[] dingPhrases = DingPhrases.getHighlightedPhrases(mContext);
            for (String phrase : dingPhrases) {
                if (!phrase.equals("")) {
                    Log.i(TAG, "Checking highlights against " + phrase);
                    if (StringProcessor.containsInsensitive(msg.getBody(), phrase)) {
                        if (!msg.getFrom().getName().equals(mCharName)) {
                            String title = phrase + " mentioned";
                            String text = msg.getFrom().getName() + " mentioned " + phrase + " in their post: " + post.poseString();
                            chatNotification(title, text);
                            break;
                        }
                    }
                }
            }
        }
    }

    private void chatNotification(String title, String text) {
        Notifications.showNotification(
                mContext,
                App.CHANNEL_CHAT,
                title,
                text,
                text,
                Notifications.getDefaultPendingIntent(
                        mContext,
                        Notifications.getDefaultNotificationIntent(mContext)
                ),
                null,
                false
        );
    }

    private void eventUserlistUpdate(UserlistUpdate update) {
        User user;
        Add add = update.getAdd();
        if (add != null) {
            String gender = add.getGender().getTitle();
            String nameColour = add.getNameColor();
            String charName = add.getCharName();
            Log.i(TAG, charName + " joined");
            String status = add.getStatus();
            if (status == null) {
                status = "online";
            }
            user = new User(
                    add.getSessionId(),
                    add.getCharId(),
                    mRepository.findIconByUserName(add.getCharName()), // eyecon, this will be changed later
                    add.getCharName(),
                    nameColour,
                    "", // icon?? What was I thinking?!
                    gender,
                    status // status?
            );
            if (HighlightedUsers.isNameHighlighted(mContext, charName)) {
                if (!charName.equals(mCharName)) {
                    // Need to check if the user is in the database
                    if (mRepository.findUser(add.getCharId()) == null) {
                        String title = charName + " is online";
                        String text = charName + " just came online";
                        chatNotification(title, text);
                    }
                }
            }
            mRepository.insertUser(user);
        }
    }

    private void eventUserlistRemove(UserlistRemove remove) {
        Log.i(TAG, remove.getRemove() + " session id left");
        int sessionId = remove.getRemove();
        Log.i(TAG, "Remove the session " + sessionId);
        mRepository.deleteUserBySessionId(sessionId);
    }

    private void eventMessageDelete(MessageDelete delete) {
        long messageId = delete.getDeletedMsgId();
        mRepository.deletePostById(messageId);
    }

    public void onUserIconUpdate(String name, String icon) {
        User currentUser = mRepository.findUser(name);
        if (currentUser != null) {
            if (!currentUser.getEyecon().equals(icon)) {
                currentUser.setEyecon(icon);
                mRepository.updateUserIconByName(name, icon);
            }
        }
    }

    public void onIconsDownloaded(List<SmallIcon> icons) {
        mRepository.insertAllIcons(icons);
    }

    @SuppressLint("ApplySharedPref")
    private void setLastEventID(long id) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("last_event_id", id);
        // Again we need this instantly
        editor.commit();
    }

    public void processMessage(String jsonData) {
        // This is ugly but for now it will do
        String jsonTmp = jsonData;
        try {
            new JSONArray(jsonTmp);
        } catch (JSONException e) {
            jsonTmp = "[" + jsonTmp + "]";
        }
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        ChatEvent[] tmpArray = gson.fromJson(jsonTmp, ChatEvent[].class);
        for (ChatEvent tmp : tmpArray) {
            // Convert the chat event to json
            String jsonEvent = gson.toJson(tmp);
            switch (tmp.getType()) {
                case "ROOM_CHAT": {
                    RoomChat msg = gson.fromJson(jsonEvent, RoomChat.class);
                    setLastEventID(msg.getId());
                    eventRoomChat(msg);
                    break;
                }
                case "WHISPER_CHAT": {
                    RoomChat msg = gson.fromJson(jsonEvent, RoomChat.class);
                    msg.setPrivate(true);
                    setLastEventID(msg.getId());
                    eventRoomChat(msg);
                    break;
                }
                case "USERLIST_UPDATE":
                    UserlistUpdate update = gson.fromJson(jsonEvent, UserlistUpdate.class);
                    setLastEventID(update.getId());
                    eventUserlistUpdate(update);
                    break;
                case "USERLIST_REMOVE":
                    UserlistRemove remove = gson.fromJson(jsonEvent, UserlistRemove.class);
                    setLastEventID(remove.getId());
                    eventUserlistRemove(remove);
                    break;
                case "DEL_MESSAGE":
                    MessageDelete delete = gson.fromJson(jsonEvent, MessageDelete.class);
                    setLastEventID(delete.getId());
                    eventMessageDelete(delete);
                    break;
            }
        }

    }
}
