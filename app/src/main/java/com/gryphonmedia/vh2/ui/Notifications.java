package com.gryphonmedia.vh2.ui;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.text.Html;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.gryphonmedia.vh2.App;
import com.gryphonmedia.vh2.MainActivity;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.utils.Constants;

import org.jetbrains.annotations.NotNull;

public class Notifications {
    private static int currentNotificationId = 10000;

    public static Intent getDefaultNotificationIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    public static PendingIntent getDefaultPendingIntent(Context context, Intent notificationIntent) {
        return PendingIntent.getActivity(context,
                0, notificationIntent, 0);
    }

    public static int getCurrentNotificationId() {
        int oldNotificationId = currentNotificationId;
        currentNotificationId += 1;
        return oldNotificationId;
    }

    private static Intent getUrlNotificationIntent() {
        Intent notificationIntent = new Intent(Intent.ACTION_VIEW);
        notificationIntent.setData(Uri.parse(Constants.APK_URL));

        return notificationIntent;
    }

    public static Notification showNotification(
            Context context,
            String channelId,
            String title,
            String shortText,
            @Nullable String longText,
            @Nullable PendingIntent pendingIntent,
            @Nullable NotificationCompat.Action[] actions,
            boolean returnNotification
    ) {
        if (longText == null) {
            longText = shortText;
        }
        long[] pattern = { 500, 1000, 500 };

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context, channelId)
                .setLights(Color.BLUE, 500, 500)
                .setAutoCancel(true)
                .setVibrate(pattern)
                .setSound(null)
                .setContentTitle(title)
                .setContentText(shortText)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher))
                .setPriority(NotificationCompat.PRIORITY_MAX);
        if (pendingIntent != null) {
            notificationBuilder.setContentIntent(pendingIntent);
        }
        if (actions != null) {
            for (NotificationCompat.Action action : actions) {
                notificationBuilder.addAction(action);
            }
        }
        if (longText != null) {
            notificationBuilder.setStyle(new NotificationCompat.BigTextStyle()
                    .bigText(Html.fromHtml(longText)));
        }
        Notification notification = notificationBuilder.build();
        if (returnNotification) {
            return notification;
        }
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(getCurrentNotificationId(), notification);
        return notification;
    }

    public static void clearNotifications(Context context)
    {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.cancelAll();
    }

    public static void clearNotifications(Context context, @NotNull int[] ids)
    {
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        for (int id: ids) {
            notificationManager.cancel(id);
        }
    }

    public static void showUpdateNotification(Context context) {
        Intent notificationIntent = Notifications.getUrlNotificationIntent();
        PendingIntent pendingIntent = Notifications.getDefaultPendingIntent(context, notificationIntent);

        showNotification(context,
                App.CHANNEL_UPDATES,
                "New Update Available",
                "A new update is available, tap this notification to download",
                null,
                pendingIntent,
                null,
                false
        );
    }

    public static void showHttpFailureNotification(Context context) {
        Intent notificationIntent = Notifications.getDefaultNotificationIntent(context);
        PendingIntent pendingIntent = Notifications.getDefaultPendingIntent(context, notificationIntent);

        showNotification(context,
                App.CHANNEL_CHAT,
                "Request failed",
                "It seems the chat is down or there's a network connection issue",
                null,
                pendingIntent,
                null,
                false
        );
    }
}
