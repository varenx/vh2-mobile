package com.gryphonmedia.vh2.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gryphonmedia.vh2.MainActivity;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.User;
import com.gryphonmedia.vh2.utils.HighlightedUsers;
import com.gryphonmedia.vh2.utils.IgnoredUsers;
import com.squareup.picasso.Picasso;

import java.util.List;

public class OnlineAdapter extends RecyclerView.Adapter<OnlineAdapter.ViewHolder> {

    private final List<User> mUsers;
    private final Context mContext;

    public interface OnItemLongClickListener {
    }

    public OnlineAdapter(List<User> mUsers, Context mContext) {
        this.mUsers = mUsers;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.online_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final User user = getItem(position);
        holder.charName.setTextColor(Color.parseColor(user.getColour()));
        holder.charName.setText(user.getName());
        String status = user.getStatus();
        if (status.equals("")) {
            status = "online";
        }
        holder.status.setText(status);
        // By default, no strikethrough
        int paintFlags = holder.charName.getPaintFlags();
        paintFlags &= ~Paint.STRIKE_THRU_TEXT_FLAG;
        holder.charName.setPaintFlags(paintFlags);
        if ((user.getEyecon() != null) && (!user.getEyecon().equals(""))) {
            String url = "https://vh.mucktopia.com" + user.getEyecon();
            Picasso.get().load(url).into(holder.charIcon);
        }
        if (HighlightedUsers.isNameHighlighted(mContext, user.getName())) {
            holder.charName.setText(">> " + user.getName());
        } else if (IgnoredUsers.isUserIgnored(mContext, user.getName())) {
            holder.charName.setTextColor(Color.parseColor("#333333"));
            holder.status.setText("muted");
            holder.charName.setPaintFlags(paintFlags | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        if ((user.getGender().equals("unspecified")) || (user.getGender().equals("none"))) {
            holder.gender.setText("");
        } else {
            holder.gender.setText(user.getGender());
        }
        switch (user.getGender()) {
            case "male":
                holder.gender.setTextColor(Color.parseColor("#6597E1"));
                break;
            case "female":
                holder.gender.setTextColor(Color.parseColor("#CD36DE"));
                break;
            default:
                holder.gender.setTextColor(Color.parseColor("#FFFFFF"));
                break;
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                ((MainActivity)mContext).onItemLongClicked(position);
                return true;
            }
        });
    }

    public User getItem(int position) {
        return mUsers.get(position);
    }

    @Override
    public int getItemCount() {
        return mUsers.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView charIcon;
        TextView charName;
        TextView status;
        TextView gender;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            charIcon = itemView.findViewById(R.id.charIcon);
            charName = itemView.findViewById(R.id.charName);
            status = itemView.findViewById(R.id.status);
            gender = itemView.findViewById(R.id.gender);
        }
    }
}
