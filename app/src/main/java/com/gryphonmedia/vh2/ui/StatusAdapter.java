package com.gryphonmedia.vh2.ui;


import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.models.Status;

import java.util.List;

public class StatusAdapter extends ArrayAdapter<Status> {

    private LayoutInflater inflater;
    private final List<Status> mStatuses;

    public StatusAdapter(@NonNull Context context, int resource, @NonNull List<Status> objects) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        mStatuses = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    public int getPositionByCode(String code) {
        int position = -1;
        for (int i = 0; i < mStatuses.size(); i += 1) {
            if (mStatuses.get(i).getCode().equals(code)) {
                position = i;
                break;
            }
        }
        return position;
    }

    @SuppressLint("InflateParams")
    private View initView(int position, @Nullable View v) {
        if (v == null) {
            v = inflater.inflate(R.layout.statuses_list_item, null, true);
        }
        TextView statusName = v.findViewById(R.id.status);
        if (mStatuses.get(position) != null) {
            statusName.setText(mStatuses.get(position).getName());
        }
        return v;
    }
}
