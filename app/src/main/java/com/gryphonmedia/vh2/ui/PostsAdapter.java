package com.gryphonmedia.vh2.ui;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.Post;
import com.gryphonmedia.vh2.fragments.RoomFragment;
import com.gryphonmedia.vh2.utils.StringProcessor;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PostsAdapter extends RecyclerView.Adapter<PostsAdapter.ViewHolder> {

    private static final String TAG = "PostsAdapter";

    private final List<Post> mPosts;
    private final RoomFragment fragment;

    public interface OnItemLongClickListener {
    }

    public PostsAdapter(List<Post> mPosts, RoomFragment fragment) {
        this.mPosts = mPosts;
        this.fragment = fragment;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.posts_list_item, parent, false);
        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        final Post post = mPosts.get(position);
        if (post != null) {
            holder.msg_timestamp.setText(post.getMsgTime());
            if (post.isPose()) {
                holder.msg_body.setText(post.getSender() + " " + StringProcessor.processHtml(post.getText()));
            } else {
                holder.msg_body.setText(StringProcessor.processHtml(post.getText()));
            }
            try {
                holder.msg_body.setTextColor(Color.parseColor(post.getTextColour()));
            } catch (IllegalArgumentException e) {
                Log.i(TAG, "Illegal colour for sender's posts [" + post.getSender() + "]: " + post.getTextColour());
            }
            holder.msg_sender.setText(post.getSender());
            try {
                holder.msg_sender.setTextColor(Color.parseColor(post.getSenderColour()));
            } catch (IllegalArgumentException e) {
                Log.i(TAG, "Illegal colour for sender's name [" + post.getSender() + "]: " + post.getSenderColour());
            }
            // Get the eyecon
            if ((post.getEyecon() != null) && !post.getEyecon().equals("")) {
                String eyeconURL = "https://vh.mucktopia.com/img/eyecon/" + post.getEyecon();
                Picasso.get().load(eyeconURL).into(holder.msg_eyecon);
                holder.msg_eyecon.setVisibility(View.VISIBLE);
            } else {
                holder.msg_eyecon.setVisibility(View.GONE);
            }
            // Set the privateInfo
            if (post.isPrivate()) {
                holder.msg_privateInfo.setVisibility(View.VISIBLE);
                holder.msg_privateInfo.setTextColor(Color.WHITE);
                holder.msg_privateInfo.setText(" >> " + post.getRecipient() + " ");
            } else {
                holder.msg_privateInfo.setVisibility(View.GONE);
            }
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                fragment.onItemLongClicked(position);
                return true;
            }
        });
    }

    public Post getItem(int position) {
        return mPosts.get(position);
    }

    @Override
    public int getItemCount() {
        return mPosts.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView msg_timestamp;
        TextView msg_sender;
        TextView msg_body;
        ImageView msg_eyecon;
        TextView msg_privateInfo;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            msg_timestamp = itemView.findViewById(R.id.msg_timestamp);
            msg_sender = itemView.findViewById(R.id.msg_sender);
            msg_body = itemView.findViewById(R.id.msg_body);
            msg_eyecon = itemView.findViewById(R.id.msg_eyecon);
            msg_privateInfo = itemView.findViewById(R.id.msg_privateInfo);
        }
    }
}
