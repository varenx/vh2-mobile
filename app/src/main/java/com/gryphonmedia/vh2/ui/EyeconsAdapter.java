package com.gryphonmedia.vh2.ui;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.models.FavColour;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EyeconsAdapter extends ArrayAdapter<FavColour> {

    private LayoutInflater inflater;
    private final List<FavColour> mEyecons;

    public EyeconsAdapter(@NonNull Context context, int resource, @NonNull List<FavColour> objects) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        mEyecons = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    public int getPositionByEyeconId(int eyeconId) {
        int position = -1;
        for (int i = 0; i < mEyecons.size(); i += 1) {
            if (mEyecons.get(i).getId() == eyeconId) {
                position = i;
                break;
            }
        }
        return position;
    }

    public int getPositionByEyeconString(String eyeconString) {
        int position = -1;
        for (int i = 0; i < mEyecons.size(); i += 1) {
            if (mEyecons.get(i).toString().equals(eyeconString)) {
                position = i;
                break;
            }
        }
        return position;
    }

    private View initView(int position, @Nullable View v) {
        if (v == null) {
            v = inflater.inflate(R.layout.eyecons_list_item, null, true);
        }
        ImageView ivEyeconImg = v.findViewById(R.id.dropdown_eyecon_img);
        TextView tvEyeconColour = v.findViewById(R.id.dropdown_eyecon_color);
        FavColour eyecon = mEyecons.get(position);
        if (eyecon != null) {
            String eyeconURL = "https://vh.mucktopia.com/img/eyecon/" + eyecon.getEyecon();
            Picasso.get().load(eyeconURL).into(ivEyeconImg);
            tvEyeconColour.setText(eyecon.getColour().toUpperCase());
            tvEyeconColour.setTextColor(Color.parseColor(eyecon.getColour()));
        }
        return v;
    }
}
