package com.gryphonmedia.vh2.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.Character;

import java.util.List;

public class CharactersAdapter extends ArrayAdapter<Character> {

    private LayoutInflater inflater;
    private final List<Character> mCharacters;

    public CharactersAdapter(@NonNull Context context, int resource, @NonNull List<Character> objects) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        mCharacters = objects;
    }

    public int getPositionByName(String name) {
        int position = -1;
        for (int i = 0; i < mCharacters.size(); i += 1) {
            if (mCharacters.get(i).getName().equals(name)) {
                position = i;
                break;
            }
        }
        return position;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    @SuppressLint("InflateParams")
    private View initView(int position, @Nullable View v) {
        if (v == null) {
            v = inflater.inflate(R.layout.characters_list_item, null, true);
        }
        TextView charName = v.findViewById(R.id.charName);
        if (mCharacters.get(position) != null) {
            charName.setText(mCharacters.get(position).getName());
        }
        return v;
    }
}
