package com.gryphonmedia.vh2.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.ChatRoom;

import java.util.List;

public class RoomsAdapter extends ArrayAdapter<ChatRoom> {

    private LayoutInflater inflater;
    private final List<ChatRoom> mRooms;

    public RoomsAdapter(@NonNull Context context, int resource, @NonNull List<ChatRoom> objects) {
        super(context, resource, objects);
        inflater = LayoutInflater.from(context);
        mRooms = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position, convertView);
    }

    public int getPositionById(int id) {
        int position = -1;
        for (int i = 0; i < mRooms.size(); i += 1) {
            if (mRooms.get(i).getId() == id) {
                position = i;
                break;
            }
        }
        return position;
    }

    @SuppressLint("InflateParams")
    private View initView(int position, @Nullable View v) {
        if (v == null) {
            v = inflater.inflate(R.layout.rooms_list_item, null, true);
        }
        TextView roomName = v.findViewById(R.id.room_name);
        if (mRooms.get(position) != null) {
            roomName.setText(mRooms.get(position).getName());
        }
        return v;
    }
}
