package com.gryphonmedia.vh2.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.gryphonmedia.vh2.App;
import com.gryphonmedia.vh2.ChatEventHandler;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.SmallIcon;
import com.gryphonmedia.vh2.models.chatevents.DisconnectEvent;
import com.gryphonmedia.vh2.ui.Notifications;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ChatService extends VHBaseService {
    private static final String TAG = ChatService.class.getSimpleName();
    public static final String CHATSERVICE_LOGGEDOUT_MESSAGE = "ChatServiceLoggedOutMessage";
    public static final String ACTION_STOP_SERVICE = "StopChat";
    public static final String ACTION_START_SERVICE = "StartChat";
    public static final String ACTION_SWITCH_TO_POLL_SERVICE = "SwitchToPollChat";
    public static final String ACTION_SWITCH_TO_REALTIME_SERVICE = "SwitchToRealTimeChat";

    private HandlerThread updaterThread;
    private Handler updaterHandler;
    private Runnable updaterRunnable;
    private ChatEventHandler eventHandler;
    private long lastEventId;
    private long lastUserlistDownloadTimestamp = 0;
    private boolean pollingRunning = true;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private String pollChat() {
        fetchChatData();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ChatService.this);
        lastEventId = prefs.getLong("last_event_id", 0);
        return ajax.r("events.json?lastEventId=" + lastEventId, null, null);
    }

    private void pollUserlist() {
        String userlistHtml = ajax.fetchTempUserlist();
        Pattern regex = Pattern.compile("<a href=\"#Whisper\" target=\"_blank\" data-pmtarget=\"([a-zA-Z0-9_-]+)\" data-open=\"whisper\">\\s*" +
                "<img class=\"tile-avatar pull-left\" src=\"([^\"]+)\">\\s*" +
                "</a>\\s*", Pattern.MULTILINE);
        Matcher matcher = regex.matcher(userlistHtml);
        List<SmallIcon> icons = new ArrayList<>();
        while (matcher.find()) {
            String username = matcher.group(1);
            String eyeconUrl = matcher.group(2);
            icons.add(new SmallIcon(username, eyeconUrl));
            if (eventHandler == null) {
                eventHandler = new ChatEventHandler(ChatService.this);
            }
            eventHandler.onUserIconUpdate(username, eyeconUrl);
        }
        eventHandler.onIconsDownloaded(icons);
    }

    private int getPollingSpeed() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(ChatService.this);
        return prefs.getInt("polling_speed", 5000);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    private Notification fgNotification() {
        // Create the notification here...
        PendingIntent pendingIntent = Notifications.getDefaultPendingIntent(
                this,
                Notifications.getDefaultNotificationIntent(this)
        );

        // Create an action for logging out
        Intent logoutService = new Intent(this, LogoutService.class);
        PendingIntent pStopSelf = PendingIntent.getService(this, 0, logoutService, 0);
        NotificationCompat.Action actionLogout = new NotificationCompat.Action.Builder(R.drawable.ic_launcher_background, getString(R.string.sign_out), pStopSelf).build();

        return Notifications.showNotification(
                this,
                App.CHANNEL_FG_SERVICE,
                getString(R.string.app_name),
                "Click this to return to the app",
                null,
                pendingIntent,
                new NotificationCompat.Action[] {actionLogout},
                true
        );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        if (intent == null || ACTION_START_SERVICE.equals(intent.getAction())) {
            // Start service
            startChatService();
        } else {
            Log.i(TAG, "Received action " + intent.getAction());
            if (ACTION_STOP_SERVICE.equals(intent.getAction())) {
                // Stop service
                shutdownChatService();
            }
            // TODO: Need to add back the real time service when I figure out how...
        }

        return START_STICKY;
    }

    private void onChatDisconnected() {
        shutdownChatService();
        // Send broadcast message to activity, we need to switch back to
        // character login if the activity is on the screen
        // Now we broadcast a message that we're done
        Intent messageIntent = new Intent(CHATSERVICE_LOGGEDOUT_MESSAGE);
        PendingIntent pendingIntent = Notifications.getDefaultPendingIntent(
                this,
                Notifications.getDefaultNotificationIntent(this)
        );
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.sendBroadcast(messageIntent);
        Notifications.showNotification(this,
                App.CHANNEL_CHAT,
                "Disconnected",
                "It looks like we got disconnected from the chat.",
                null,
                pendingIntent,
                null,
                false
        );
    }

    private void startChatService() {
        updaterThread = new HandlerThread("UpdaterThread");
        updaterThread.start();
        updaterHandler = new Handler(updaterThread.getLooper());
        updaterRunnable = new Runnable() {
            @Override
            public void run() {
                String eventsData = pollChat();
                @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String currentDateAndTime = sdf.format(new Date());
                Log.i(TAG, "Polling chat " + currentDateAndTime);
                try {
                    // Let's check if it's a disconnected error
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    gsonBuilder.setPrettyPrinting();
                    Gson gson = gsonBuilder.create();
                    DisconnectEvent tmpEvent = gson.fromJson(eventsData, DisconnectEvent.class);
                    if (tmpEvent != null && tmpEvent.getError() != null && tmpEvent.getError().equals("NotLoggedIn")) {
                        Log.i(TAG, "Disconnected from chat");
                        onChatDisconnected();
                        return; // We no longer wish to run this method again
                    }
                } catch (JsonSyntaxException e) {
                    // We do nothing here, the json syntax exception just means we got an
                    // array and not a 'not logged in' error
                }
                if (eventHandler == null) {
                    eventHandler = new ChatEventHandler(ChatService.this);
                }
                eventHandler.processMessage(eventsData);
                // Let's check userlist download
                long currentTimestamp = new Date().getTime();
                if (currentTimestamp >= (lastUserlistDownloadTimestamp + getPollingSpeed() * 10)) {
                    // Get the userlist too
                    lastUserlistDownloadTimestamp = currentTimestamp;
                    pollUserlist();
                }
                if (pollingRunning) {
                    updaterHandler.postDelayed(this, getPollingSpeed());
                }
            }
        };
        updaterHandler.postDelayed(updaterRunnable, 1000);
        startForeground(1, fgNotification());
    }

    private void shutdownChatService() {
        if ((updaterHandler != null) && (updaterRunnable != null)) {
            updaterHandler.removeCallbacks(updaterRunnable);

            updaterRunnable = null;
            updaterHandler = null;

            pollingRunning = false;
        }
        stopForeground(true);
        stopSelf();
    }

    @Override
    public void onDestroy() {
        // Stop the running thread
        shutdownChatService();
        stopSelf();
        super.onDestroy();
    }
}
