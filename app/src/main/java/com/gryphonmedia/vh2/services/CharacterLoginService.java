package com.gryphonmedia.vh2.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gryphonmedia.vh2.database.Post;
import com.gryphonmedia.vh2.database.Repository;
import com.gryphonmedia.vh2.database.User;
import com.gryphonmedia.vh2.models.FavColour;
import com.gryphonmedia.vh2.models.InitialFavColours;
import com.gryphonmedia.vh2.models.InitialPost;
import com.gryphonmedia.vh2.models.InitialUser;
import com.gryphonmedia.vh2.models.SessionSettings;
import com.gryphonmedia.vh2.models.chatevents.RoomChat;
import com.gryphonmedia.vh2.models.chatevents.update.Add;
import com.gryphonmedia.vh2.utils.Constants;
import com.gryphonmedia.vh2.utils.DingPhrases;
import com.gryphonmedia.vh2.utils.Eyecons;
import com.gryphonmedia.vh2.utils.IgnoredUsers;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.Cookie;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CharacterLoginService extends IntentService {

    private static final String TAG = CharacterLoginService.class.getSimpleName();
    public static final String MY_SERVICE_MESSAGE = "CharacterLoginServiceMessage";

    private String responseText;
    OkHttpClient client;
    ClearableCookieJar cookieJar;
    private Integer roomId = 2;
    private Repository mRepository;
    private String[] nameDingPatterns;
    private FavColour[] favColours;

    public CharacterLoginService() {
        super(TAG);
    }

    private void setupClient() {
        cookieJar = new PersistentCookieJar(new SetCookieCache(), new SharedPrefsCookiePersistor(getApplicationContext()));
        client = new OkHttpClient.Builder().cookieJar(cookieJar).build();
    }

    private String loginCharacter(String charName, Integer roomId) {
        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("command", "charLogin");
        formBody.add("room", String.valueOf(roomId));
        formBody.add("charName", charName);
        RequestBody requestBody = formBody.build();
        Request.Builder requestBuilder = new Request.Builder()
                .url("https://vh.mucktopia.com/account.srv")
                .header("User-Agent", Constants.BROWSER);
        Request request = requestBuilder.method("POST", requestBody).build();
        String chatUrl = "";
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
            chatUrl = response.request().url().toString();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return fetchChatRoomContent(chatUrl);
    }

    private void initialPosts(String res) {
        Log.i(TAG, "INITIAL POSTS: " + res);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        InitialPost result = gson.fromJson(res, InitialPost.class);
        for (RoomChat msg: result.getResult().getInitialChat()) {
            if (IgnoredUsers.isUserIgnored(getApplicationContext(), msg.getFrom().getName())) {
                // The user is ignored, so ignore them!
                continue;
            }
            if (msg.getType().equals("WHISPER_CHAT")) {
                msg.setPrivate(true);
            }
            Post post = new Post(
                    msg.getId(),
                    msg.getBody(),
                    msg.getFrom().getName(),
                    msg.getFrom().getPrettyName(),
                    msg.getTo().getName(),
                    msg.getTo().getPrettyName(),
                    msg.getTextColor(),
                    msg.getEyecon(),
                    msg.getMsgTime(),
                    msg.getMsgDate(),
                    msg.isPose(),
                    msg.isPrivate()
            );
            Log.i(TAG, post.toString());
            mRepository.insertPost(post);
        }
    }

    private void initialUserList(String res) {
        Log.i(TAG, "INITIAL USERLIST: " + res);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        InitialUser result = gson.fromJson(res, InitialUser.class);
        for (Add add: result.getResult()) {
            String gender = add.getGender().getTitle();
            String nameColour = add.getNameColor();
            String charName = add.getCharName();
            String status = add.getStatus();
            if (status == null) {
                status = "online";
            }
            Log.i(TAG, charName + " joined");
            User user = new User(
                    add.getSessionId(),
                    add.getCharId(),
                    mRepository.findIconByUserName(add.getCharName()), // eyecon, this will be changed later
                    add.getCharName(),
                    nameColour,
                    "", // icon?? What was I thinking?!
                    gender,
                    status // status?
            );
            mRepository.insertUser(user);
        }
    }

    private void initialFavColours(String res) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        InitialFavColours favColourRes = gson.fromJson(res, InitialFavColours.class);
        favColours = favColourRes.getResult();
    }

    private String fetchChatRoomContent(String chatUrl) {
        // First of all we need the chat ID
        String chatId;
        Pattern regex = Pattern.compile("<script id=\"session-settings\" " +
                "type=\"application/json\"\\s*>([^<]+)</script>", Pattern.MULTILINE);
        Matcher matcher = regex.matcher(responseText);
        if (!matcher.find()) {
            return "";
        }
        // We should have a JSON string at this point, so we create GsonBuilder
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        Gson gson = gsonBuilder.create();
        SessionSettings session = gson.fromJson(matcher.group(1), SessionSettings.class);
        String csrf = session.getMe().getCsrf();
        nameDingPatterns = session.getMe().getSettings().getNameDingPatterns();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("csrf", csrf);
        editor.apply();

        // Now we look up our session ID
        regex = Pattern.compile("https://vh\\.mucktopia\\.com/u/([^/]+)/jchat\\.srv");
        matcher = regex.matcher(chatUrl);
        if (!matcher.find()) {
            return "";
        }
        chatId = matcher.group(1);

        Log.i(TAG, "chat ID: " + chatId);
        Request.Builder builder = new Request.Builder()
                .url("https://vh.mucktopia.com/u/" + chatId + "/jchat.srv")
                .header("User-Agent", Constants.BROWSER);
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpUrl url = HttpUrl.get("https://vh.mucktopia.com/u/" + chatId);
        List<Cookie> cookies = cookieJar.loadForRequest(url);
        StringBuilder strCookieString = new StringBuilder();
        for (Cookie cookie : cookies) {
            if (!strCookieString.toString().equals("")) {
                strCookieString.append("; ");
            }
            strCookieString.append(cookie.name()).append("=").append(cookie.value());
        }
        editor.putString("chatcookies", strCookieString.toString());
        editor.commit();
        // Let's grab the chat room contents
        // IMPORTANT: We need to change the room before we go and fetch any other info
        //------------------------------------------
        FormBody.Builder formBody = new FormBody.Builder();
        // And this is how we post a message
        formBody.add("action", "changeRoom");
        formBody.add("roomId", String.valueOf(roomId));
        formBody.add("csrf", csrf);
        // "chatcookies" has the normal chat cookies (vhchatses?!)

        RequestBody requestBody = formBody.build();
        Request.Builder requestBuilder = new Request.Builder()
                .url("https://vh.mucktopia.com/u/" + chatId + "/action.srv")
                .post(requestBody)
                .header("Referer", "https://vh.mucktopia.com/u/" + chatId + "/jchat.srv")
                .header("Content-Type", "application/x-www-form-urlencoded;charset=UTF8")
                .header("Origin", "https://vh.mucktopia.com")
                .header("User-Agent", Constants.BROWSER)
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Cookie", strCookieString.toString());
        request = requestBuilder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //------------------------------------------
        // Initial chat room load
        // TODO: This should really be done in a single call with initialchat-all
        mRepository.wipeRoomData();
        builder = new Request.Builder()
                .header("Cookie", strCookieString.toString())
                .header("User-Agent", Constants.BROWSER)
                .url("https://vh.mucktopia.com/u/" + chatId + "/info.srv?dataset=initialchat-room");
        request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
            initialPosts(responseText);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Load up the userlist
        builder = new Request.Builder()
                .header("Cookie", strCookieString.toString())
                .header("User-Agent", Constants.BROWSER)
                .url("https://vh.mucktopia.com/u/" + chatId + "/info.srv?dataset=userlist");
        request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
            initialUserList(responseText);
        } catch (IOException e) {
            e.printStackTrace();
        }
        // Fetch the eyecons
        builder = new Request.Builder()
                .header("Cookie", strCookieString.toString())
                .header("User-Agent", Constants.BROWSER)
                .url("https://vh.mucktopia.com/u/" + chatId + "/info.srv?dataset=favcolors");
        request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
            initialFavColours(responseText);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return chatId;
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        mRepository = Repository.getInstance(getApplicationContext());
        mRepository.wipeRoomData();
        setupClient();
        String charName = "";
        if (intent != null) {
            charName = intent.getStringExtra("charName");
        }
        if (intent != null) {
            roomId = intent.getIntExtra("roomId", 2);
        }
        Log.i(TAG, "Logging in as " + charName + " to roomID " + roomId);
        String chatId = loginCharacter(charName, roomId);
        HttpUrl url = HttpUrl.get("https://vh.mucktopia.com/u/" + chatId);
        List<Cookie> cookies = cookieJar.loadForRequest(url);
        StringBuilder strCookieString = new StringBuilder();
        for (Cookie cookie : cookies) {
            if (!strCookieString.toString().equals("")) {
                strCookieString.append("; ");
            }
            strCookieString.append(cookie.name()).append("=").append(cookie.value());
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("chatId", chatId);
        editor.putString("cookies", strCookieString.toString());
        editor.putString("current_character", charName);
        editor.putString("current_status", "online");
        // Wipe the ding phrases, these should be loaded from the chat
        editor.putString("dingphrases", "");
        // Wipe our eyecons, these are loaded from the chat, too
        editor.putString("eyecons", "");
        editor.apply();
        for (String dingPhrase : nameDingPatterns) {
            DingPhrases.highlightPhrase(getApplicationContext(), dingPhrase);
        }
        for (FavColour favColour: favColours) {
            if (favColour != null) {
                Log.i(TAG, "Adding eyecon: " + favColour.toString());
                Eyecons.addEyecon(this, favColour);
            }
        }
        if (favColours[0] != null) {
            editor.putString("current_eyecon", favColours[0].toString());
            editor.putInt("current_eyecon_id", favColours[0].getId());
        }
        editor.commit();
        // Now we broadcast a message that we're done
        Intent messageIntent = new Intent(MY_SERVICE_MESSAGE);
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.sendBroadcast(messageIntent);
        stopSelf();
    }
}
