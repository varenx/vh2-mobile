package com.gryphonmedia.vh2.services;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.Arrays;
import java.util.HashMap;

public class SetStatusService extends VHIntentService {

    private static final String TAG = SetStatusService.class.getSimpleName();
    public static final String MY_SERVICE_MESSAGE = "SetStatusServiceMessage";

    public SetStatusService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        fetchChatData();
        String status = "";
        if (intent != null) {
            status = intent.getStringExtra("status");
        }
        HashMap<String, String> params = new HashMap<>();
        // Available statuses
        String[] statuses = { "away", "pred", "ooc", "online", "long", "lfrp", "ic", "dnd", "distracted", "prey" };
        // Let's check if it's a valid status, if not, we simply change to online
        if (!Arrays.asList(statuses).contains(status)) {
            status = "online";
        }
        params.put("newStatus", status);
        ajax.defaultAjaxAction("setStatus", params);
        // Set the current status in prefs
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("current_status", status);
        editor.apply();
        // Now we broadcast a message that we're done
        Intent messageIntent = new Intent(MY_SERVICE_MESSAGE);
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.sendBroadcast(messageIntent);
        stopSelf();
    }
}
