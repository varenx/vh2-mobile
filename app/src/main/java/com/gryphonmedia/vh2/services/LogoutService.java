package com.gryphonmedia.vh2.services;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class LogoutService extends VHIntentService {

    private static final String TAG = LogoutService.class.getSimpleName();
    public static final String MY_SERVICE_MESSAGE = "LogoutServiceMessage";

    public LogoutService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        fetchChatData();
        Log.i(TAG, "Log out");
        ajax.defaultAjaxAction("logout", null);
        // Now we broadcast a message that we're done
        Intent messageIntent = new Intent(MY_SERVICE_MESSAGE);
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
        manager.sendBroadcast(messageIntent);
        stopSelf();
    }
}
