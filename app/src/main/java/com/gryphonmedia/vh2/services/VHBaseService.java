package com.gryphonmedia.vh2.services;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gryphonmedia.vh2.utils.Ajax;

abstract public class VHBaseService extends Service {

    protected Ajax ajax;
    protected String chatId;
    protected String cookies;
    protected String csrf;
    protected String charName;

    public VHBaseService() {
        super();
    }

    protected void fetchChatData() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        chatId = prefs.getString("chatId", "");
        cookies = prefs.getString("chatcookies", "");
        csrf = prefs.getString("csrf", "");
        charName = prefs.getString("current_character", "");
        ajax = new Ajax(this, chatId, csrf, cookies);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }
}
