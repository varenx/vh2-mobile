package com.gryphonmedia.vh2.services;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;

import java.util.HashMap;

public class PostMessageService extends VHIntentService {
    private static final String TAG = "PostMessageService";

    public PostMessageService() {
        super(TAG);
    }

    private String sendMessage(String postMessage, @Nullable String to, int eyeconId) {
        String strTo = (to == null) ? "" : to;

        HashMap<String, String> params = new HashMap<>();
        params.put("action", "post");
        params.put("msgBody", postMessage);
        params.put("color", String.valueOf(eyeconId));
        params.put("msgTarget", strTo);
        return ajax.defaultAjaxAction("post", params);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        fetchChatData();
        String postMessage = "";
        String msgTarget = "";
        int eyeconId = 1;
        if (intent != null) {
            postMessage = intent.getStringExtra("postMessage");
            msgTarget = intent.getStringExtra("msgTarget");
            eyeconId = intent.getIntExtra("eyeconId", 1);
        }
        Log.i(TAG, sendMessage(postMessage, msgTarget, eyeconId));
        stopSelf();
    }
}
