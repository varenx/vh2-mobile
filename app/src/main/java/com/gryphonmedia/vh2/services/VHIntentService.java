package com.gryphonmedia.vh2.services;

import android.app.IntentService;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.gryphonmedia.vh2.utils.Ajax;

abstract public class VHIntentService extends IntentService {

    protected Ajax ajax;
    protected String chatId;
    protected String cookies;
    protected String csrf;
    protected String charName;

    public VHIntentService(String name) {
        super(name);
    }

    protected void fetchChatData() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        chatId = prefs.getString("chatId", "");
        cookies = prefs.getString("chatcookies", "");
        csrf = prefs.getString("csrf", "");
        charName = prefs.getString("current_character", "");
        ajax = new Ajax(this, chatId, csrf, cookies);
    }
}