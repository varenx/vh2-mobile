package com.gryphonmedia.vh2.services;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.gryphonmedia.vh2.database.Character;
import com.gryphonmedia.vh2.database.ChatRoom;
import com.gryphonmedia.vh2.database.Repository;
import com.gryphonmedia.vh2.utils.HttpHelper;

import java.io.IOException;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class LoginService extends IntentService {

    private static final String TAG = LoginService.class.getSimpleName();
    public static final String MY_SERVICE_MESSAGE = "LoginServiceMessage";
    public static final String MY_SERVICE_MESSAGE_FAIL = "LoginServiceMessageFailure";

    private String username;
    private String password;
    private String responseText;
    OkHttpClient client;
    private Repository mRepository;
    private boolean loginRerun = false;

    public LoginService() {
        super("LoginService");
    }

    private void setupClient() {
        client = HttpHelper.clientWithCookieJar(getApplicationContext());
    }

    private void loginStep1() {
        setupClient();
        mRepository.wipeRoomData();
        Request.Builder builder = new Request.Builder()
                .url("https://vh.mucktopia.com/");
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return;
            }
            responseText = Objects.requireNonNull(response.body()).string();
            loginStep2();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loginStep2() {
        Request.Builder builder = new Request.Builder()
                .url("https://vh.mucktopia.com/login.srv?mode=gotoSso&provider=vh.mucktopia.com");
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return;
            }
            responseText = Objects.requireNonNull(response.body()).string();
            loginStep3();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loginStep3() {
        // Get our redirection url
        String redirectionUrl;
        Pattern regex = Pattern.compile("name=\"redir\" value=\"([^\"]+)\"");
        Matcher matcher = regex.matcher(responseText);
        // Fetch the redirection URL
        if (!matcher.find()) {
            return;
        }
        redirectionUrl = matcher.group(1);
        // Make sure to replace the &amp; in the url to correctly encode
        redirectionUrl = redirectionUrl.replace("&amp;", "&");
        FormBody.Builder formBody = new FormBody.Builder();
        formBody.add("username", username);
        formBody.add("password", password);
        formBody.add("redir_url", redirectionUrl);
        RequestBody requestBody = formBody.build();
        Request.Builder requestBuilder = new Request.Builder()
                .url("https://vh.mucktopia.com/auth/login.srv?mode=login");
        Request request = requestBuilder.method("POST", requestBody).build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return;
            }
            responseText = Objects.requireNonNull(response.body()).string();
            if (!loginRerun) {
                loginRerun = true;
                loginStep1();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "After logging in: " + responseText);
    }

    private boolean verifyLogin() {
        Request.Builder builder = new Request.Builder()
                .url("https://vh.mucktopia.com/account.srv");
        Request request = builder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                // Handle this error
                return false;
            }
            responseText = Objects.requireNonNull(response.body()).string();
            Log.i(TAG, "Verifying login...");
            Log.i(TAG, responseText);
            if (!responseText.contains("Welcome " + username)) {
                return false;
            }
            // Fetch the list of characters we have
            fetchCharacters(responseText);
            fetchRooms(responseText);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void fetchCharacters(String html)
    {
        String strRegex = "<button class=\"btn btn-default btn-block\"\\s+" +
                "for=\"charName_([0-9]+)\"\\s+name=\"charName\"\\s+value=\"([a-zA-Z0-9_]+)\"" +
                "\\s+style=\"border-top-right-radius:0;border-bottom-right-radius: 0;\">";
        Pattern regex = Pattern.compile(strRegex);
        Matcher matcher = regex.matcher(html);
        if (!matcher.find()) {
            Log.i(TAG, "ERROR: No characters found");
            return;
        }
        matcher.reset();
        while (matcher.find()) {
            Log.i(TAG, matcher.group(1) + " " + matcher.group(2));
            // Now we just need to add these characters to the database
            mRepository.insertCharacter(new Character(Integer.parseInt(matcher.group(1)), matcher.group(2)));
        }
    }

    private void fetchRooms(String html)
    {
        String strRegex = "name=\"room\"\\s+value=\"(\\d+)\">\\s+(.*?)</label>";
        Pattern regex = Pattern.compile(strRegex);
        Matcher matcher = regex.matcher(html);
        if (!matcher.find(0)) {
            Log.i(TAG, "ERROR: No rooms found");
            return;
        }
        matcher.reset();
        while (matcher.find()) {
            Log.i(TAG, matcher.group(1) + " " + matcher.group(2));
            // Add the rooms to the database
            mRepository.insertChatRoom(new ChatRoom(Integer.parseInt(matcher.group(1)), matcher.group(2)));
        }
    }

    @Override
    protected void onHandleIntent(@androidx.annotation.Nullable Intent intent) {
        mRepository = Repository.getInstance(getApplicationContext());

        username = "";
        password = "";

        if (intent != null) {
            username = intent.getStringExtra("username");
            password = intent.getStringExtra("password");
        }

        // Let's perform the login procedure
        // First request is going to be to the vh homepage so we have
        // the baseline session set up
        loginStep1();
        if (verifyLogin()) {
            // Save the username and password in our shared preferences
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("username", username);
            editor.putString("password", password);
            editor.apply();
            // Now we broadcast a message that we're done
            Intent messageIntent = new Intent(MY_SERVICE_MESSAGE);
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
            manager.sendBroadcast(messageIntent);
        } else {
            Intent messageIntent = new Intent(MY_SERVICE_MESSAGE_FAIL);
            LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getApplicationContext());
            manager.sendBroadcast(messageIntent);
        }
        stopSelf();
    }
}
