package com.gryphonmedia.vh2.models;

public class SessionSettings {
    SessionData me;
    SessionDetails ses;

    public SessionData getMe() {
        return me;
    }

    public void setMe(SessionData me) {
        this.me = me;
    }

    public SessionDetails getSes() {
        return ses;
    }

    public void setSes(SessionDetails ses) {
        this.ses = ses;
    }
}
