package com.gryphonmedia.vh2.models;

import com.gryphonmedia.vh2.models.chatevents.update.Add;

public class InitialUser {
    private Add[] result;

    public Add[] getResult() {
        return result;
    }

    public void setResult(Add[] result) {
        this.result = result;
    }
}
