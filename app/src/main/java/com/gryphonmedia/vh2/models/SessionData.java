package com.gryphonmedia.vh2.models;

public class SessionData {
    private int sessId;
    private int charId;
    private int acctId;
    private String name;
    private String csrf;
    private SessionConfig settings;

    public int getSessId() {
        return sessId;
    }

    public void setSessId(int sessId) {
        this.sessId = sessId;
    }

    public int getCharId() {
        return charId;
    }

    public void setCharId(int charId) {
        this.charId = charId;
    }

    public int getAcctId() {
        return acctId;
    }

    public void setAcctId(int acctId) {
        this.acctId = acctId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCsrf() {
        return csrf;
    }

    public void setCsrf(String csrf) {
        this.csrf = csrf;
    }

    public SessionConfig getSettings() {
        return settings;
    }

    public void setSettings(SessionConfig settings) {
        this.settings = settings;
    }
}
