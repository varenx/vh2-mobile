package com.gryphonmedia.vh2.models;

public class SessionConfig {
    private boolean hideEyecons;
    private boolean nameDing;
    private boolean enableSound;
    private String[] nameDingPatterns;
    private boolean clearWhisper;
    private boolean autoHoverGroups;
    private boolean enableTicker;
    private boolean autoHoverWhispers;

    public boolean isHideEyecons() {
        return hideEyecons;
    }

    public void setHideEyecons(boolean hideEyecons) {
        this.hideEyecons = hideEyecons;
    }

    public boolean isNameDing() {
        return nameDing;
    }

    public void setNameDing(boolean nameDing) {
        this.nameDing = nameDing;
    }

    public boolean isEnableSound() {
        return enableSound;
    }

    public void setEnableSound(boolean enableSound) {
        this.enableSound = enableSound;
    }

    public String[] getNameDingPatterns() {
        return nameDingPatterns;
    }

    public void setNameDingPatterns(String[] nameDingPatterns) {
        this.nameDingPatterns = nameDingPatterns;
    }

    public boolean isClearWhisper() {
        return clearWhisper;
    }

    public void setClearWhisper(boolean clearWhisper) {
        this.clearWhisper = clearWhisper;
    }

    public boolean isAutoHoverGroups() {
        return autoHoverGroups;
    }

    public void setAutoHoverGroups(boolean autoHoverGroups) {
        this.autoHoverGroups = autoHoverGroups;
    }

    public boolean isEnableTicker() {
        return enableTicker;
    }

    public void setEnableTicker(boolean enableTicker) {
        this.enableTicker = enableTicker;
    }

    public boolean isAutoHoverWhispers() {
        return autoHoverWhispers;
    }

    public void setAutoHoverWhispers(boolean autoHoverWhispers) {
        this.autoHoverWhispers = autoHoverWhispers;
    }
}
