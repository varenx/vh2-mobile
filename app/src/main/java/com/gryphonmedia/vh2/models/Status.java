package com.gryphonmedia.vh2.models;

import java.util.ArrayList;
import java.util.List;

public class Status {
    private String name;
    private String code;

    public Status(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static List<Status> getStatuses()
    {
        List<Status> statuses = new ArrayList<>();
        statuses.add(new Status("Online", "online"));
        statuses.add(new Status("Away", "away"));
        statuses.add(new Status("Pred", "pred"));
        statuses.add(new Status("OOC", "ooc"));
        statuses.add(new Status("Long-term RP", "long"));
        statuses.add(new Status("LFRP", "lfrp"));
        statuses.add(new Status("IC", "ic"));
        statuses.add(new Status("Do Not Disturb", "dnd"));
        statuses.add(new Status("Distracted", "distracted"));
        statuses.add(new Status("Prey", "prey"));
        return statuses;
    }
}
