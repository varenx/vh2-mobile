package com.gryphonmedia.vh2.models;

public class InitialFavColours {
    FavColour[] result;

    public FavColour[] getResult() {
        return result;
    }

    public void setResult(FavColour[] result) {
        this.result = result;
    }
}
