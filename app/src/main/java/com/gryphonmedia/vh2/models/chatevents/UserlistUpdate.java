package com.gryphonmedia.vh2.models.chatevents;

import com.gryphonmedia.vh2.models.chatevents.update.Add;

public class UserlistUpdate {
    private long id;
    private int ts;
    private Add add;
    private String remove;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTs() {
        return ts;
    }

    public void setTs(int ts) {
        this.ts = ts;
    }

    public Add getAdd() {
        return add;
    }

    public void setAdd(Add add) {
        this.add = add;
    }

    public String getRemove() {
        return remove;
    }

    public void setRemove(String remove) {
        this.remove = remove;
    }
}
