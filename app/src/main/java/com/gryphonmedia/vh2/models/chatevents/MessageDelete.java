package com.gryphonmedia.vh2.models.chatevents;

public class MessageDelete {
    private long id;
    private long deletedMsgId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeletedMsgId() {
        return deletedMsgId;
    }

    public void setDeletedMsgId(long deletedMsgId) {
        this.deletedMsgId = deletedMsgId;
    }
}
