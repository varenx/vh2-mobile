package com.gryphonmedia.vh2.models;

import com.gryphonmedia.vh2.models.chatevents.chat.From;
import com.gryphonmedia.vh2.models.chatevents.chat.To;
import com.gryphonmedia.vh2.models.chatevents.update.Add;

public class ChatEvent {
    private String type;
    private String eyecon;
    private String toLeash;
    private String msgTime;
    private From from;
    private To to;
    private boolean isPose;
    private String body;
    private String textColor;
    private long ts;
    private String msgDate;
    private long id;
    private long deletedMsgId;
    private boolean isPrivate;
    private int remove;
    private Add add;

    public ChatEvent(String type, String eyecon, String toLeash, String msgTime, From from, To to, boolean isPose, String body, String textColor, long ts, String msgDate, long id, long deletedMsgId, boolean isPrivate, int remove, Add add) {
        this.type = type;
        this.eyecon = eyecon;
        this.toLeash = toLeash;
        this.msgTime = msgTime;
        this.from = from;
        this.to = to;
        this.isPose = isPose;
        this.body = body;
        this.textColor = textColor;
        this.ts = ts;
        this.msgDate = msgDate;
        this.id = id;
        this.deletedMsgId = deletedMsgId;
        this.isPrivate = isPrivate;
        this.remove = remove;
        this.add = add;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEyecon() {
        return eyecon;
    }

    public void setEyecon(String eyecon) {
        this.eyecon = eyecon;
    }

    public String getToLeash() {
        return toLeash;
    }

    public void setToLeash(String toLeash) {
        this.toLeash = toLeash;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public boolean isPose() {
        return isPose;
    }

    public void setPose(boolean pose) {
        isPose = pose;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public long getTs() {
        return ts;
    }

    public void setTs(long ts) {
        this.ts = ts;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDeletedMsgId() {
        return deletedMsgId;
    }

    public void setDeletedMsgId(long deletedMsgId) {
        this.deletedMsgId = deletedMsgId;
    }

    public boolean isPrivate() {
        return isPrivate;
    }

    public void setPrivate(boolean aPrivate) {
        isPrivate = aPrivate;
    }

    public int getRemove() {
        return remove;
    }

    public void setRemove(int remove) {
        this.remove = remove;
    }

    public Add getAdd() {
        return add;
    }

    public void setAdd(Add add) {
        this.add = add;
    }
}
