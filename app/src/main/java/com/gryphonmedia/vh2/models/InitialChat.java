package com.gryphonmedia.vh2.models;

import com.gryphonmedia.vh2.models.chatevents.RoomChat;

public class InitialChat {
    RoomChat[] initialChat;

    public RoomChat[] getInitialChat() {
        return initialChat;
    }

    public void setInitialChat(RoomChat[] initialChat) {
        this.initialChat = initialChat;
    }
}
