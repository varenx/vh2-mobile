package com.gryphonmedia.vh2.models;

import com.google.gson.annotations.SerializedName;

public class FavColour {
    private int id;
    private String eyecon;
    @SerializedName("color")
    private String colour;
    private String name;

    public FavColour(int id, String eyecon, String colour, String name) {
        this.id = id;
        this.eyecon = eyecon;
        this.colour = colour;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEyecon() {
        return eyecon;
    }

    public void setEyecon(String eyecon) {
        this.eyecon = eyecon;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return this.id + ":" + this.eyecon + ":" + this.colour;
    }

    public static FavColour fromString(String eyeconStr) {
        String[] parts = eyeconStr.split(":");
        return new FavColour(Integer.parseInt(parts[0]), parts[1], parts[2], "");
    }
}
