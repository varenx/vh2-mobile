package com.gryphonmedia.vh2.models.chatevents;

import com.gryphonmedia.vh2.models.chatevents.chat.From;
import com.gryphonmedia.vh2.models.chatevents.chat.To;

public class WhisperChat {
    private String eyecon;
    private String toLeash;
    private String msgTime;
    private From from;
    private To to;
    private boolean isPose;
    private String body;
    private String textColor;
    private int ts;
    private String msgDate;
    private long id;

    public String getEyecon() {
        return eyecon;
    }

    public void setEyecon(String eyecon) {
        this.eyecon = eyecon;
    }

    public String getToLeash() {
        return toLeash;
    }

    public void setToLeash(String toLeash) {
        this.toLeash = toLeash;
    }

    public String getMsgTime() {
        return msgTime;
    }

    public void setMsgTime(String msgTime) {
        this.msgTime = msgTime;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public boolean isPose() {
        return isPose;
    }

    public void setPose(boolean pose) {
        isPose = pose;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTextColor() {
        return textColor;
    }

    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    public int getTs() {
        return ts;
    }

    public void setTs(int ts) {
        this.ts = ts;
    }

    public String getMsgDate() {
        return msgDate;
    }

    public void setMsgDate(String msgDate) {
        this.msgDate = msgDate;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
