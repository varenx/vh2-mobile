package com.gryphonmedia.vh2.models.chatevents;

public class DisconnectEvent {
    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
