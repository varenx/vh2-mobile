package com.gryphonmedia.vh2.models.chatevents.chat;

public class To {
    private String prettyName;
    private String name;

    public String getPrettyName() {
        return prettyName;
    }

    public void setPrettyName(String prettyName) {
        this.prettyName = prettyName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
