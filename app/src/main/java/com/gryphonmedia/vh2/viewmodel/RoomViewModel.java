package com.gryphonmedia.vh2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gryphonmedia.vh2.database.Post;
import com.gryphonmedia.vh2.database.Repository;

import java.util.List;

public class RoomViewModel extends AndroidViewModel {
    private LiveData<List<Post>> mPosts;

    private Repository mRepository;

    public RoomViewModel(@NonNull Application application) {
        super(application);
        mRepository = Repository.getInstance(application.getApplicationContext());
        mPosts = getPosts();
    }

    public LiveData<List<Post>> getPosts() {
        mPosts = mRepository.getAllPosts();
        return mPosts;
    }
}
