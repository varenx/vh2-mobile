package com.gryphonmedia.vh2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gryphonmedia.vh2.database.Repository;
import com.gryphonmedia.vh2.database.User;

import java.util.List;

public class MainActivityViewModel extends AndroidViewModel {
    public LiveData<List<User>> mUsers;

    private Repository mRepository;

    public MainActivityViewModel(@NonNull Application application) {
        super(application);
        mRepository = Repository.getInstance(application.getApplicationContext());
        mUsers = getUsers();
    }

    private LiveData<List<User>> getUsers() {
        mUsers = mRepository.getAllUsers();
        return mUsers;
    }
}
