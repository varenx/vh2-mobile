package com.gryphonmedia.vh2.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.gryphonmedia.vh2.database.Character;
import com.gryphonmedia.vh2.database.ChatRoom;
import com.gryphonmedia.vh2.database.Repository;

import java.util.List;

public class CharacterPickerViewModel extends AndroidViewModel {
    public LiveData<List<Character>> mCharacters;
    public LiveData<List<ChatRoom>> mRooms;

    public CharacterPickerViewModel(@NonNull Application application) {
        super(application);
        Repository mRepository = Repository.getInstance(application.getApplicationContext());
        mCharacters = mRepository.mCharacters;
        mRooms = mRepository.mRooms;
    }
}
