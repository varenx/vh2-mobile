package com.gryphonmedia.vh2;

import android.app.Application;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;

public class App extends Application {
    public static final String CHANNEL_FG_SERVICE = "VH2APP";
    public static final String CHANNEL_UPDATES = "VH2UPDATES";
    public static final String CHANNEL_CHAT = "VH2CHAT";

    @Override
    public void onCreate() {
        super.onCreate();
        setupNotificationChannels();
    }

    private void setupNotificationChannels() {
        createNotificationChannel(CHANNEL_FG_SERVICE);
        createNotificationChannel(CHANNEL_UPDATES);
        createNotificationChannel(CHANNEL_CHAT);
    }

    private void createNotificationChannel(String channelName) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String channelNameReadable = "VH2 App";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            if (channelName.equals(CHANNEL_FG_SERVICE)) {
                channelNameReadable = "VH2 Chat Notifications";
            }
            if (channelName.equals(CHANNEL_UPDATES)) {
                channelNameReadable = "VH2 Chat Updates";
            }
            if (channelName.equals(CHANNEL_CHAT)) {
                channelNameReadable = "VH2 Chat";
                importance = NotificationManager.IMPORTANCE_HIGH;
            }
            NotificationChannel serviceChannel = new NotificationChannel(
                    channelName,
                    channelNameReadable,
                    importance
            );

            NotificationManager manager = getSystemService(NotificationManager.class);
            manager.createNotificationChannel(serviceChannel);
        }
    }

}
