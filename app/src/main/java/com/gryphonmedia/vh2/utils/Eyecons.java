package com.gryphonmedia.vh2.utils;

import android.content.Context;

import com.gryphonmedia.vh2.models.FavColour;

import java.util.ArrayList;

public abstract class Eyecons extends DynamicLists {
    private static final String listName = "eyecons";

    public static void addEyecon(Context context, FavColour eyecon) {
        putOnList(context, listName, eyecon.toString());
    }

    public static FavColour[] getEyecons(Context context) {
        String[] eyeconStrArray = getListArray(context, listName);
        FavColour[] favColours = new FavColour[eyeconStrArray.length];
        int index = 0;
        for (String eyeconStr: eyeconStrArray) {
            if (!eyeconStr.equals("")) {
                FavColour favColour = FavColour.fromString(eyeconStr);
                favColours[index++] = favColour;
            }
        }
        return favColours;
    }

    public static ArrayList<FavColour> getEyeconList(Context context) {
        String[] eyeconStrArray = getListArray(context, listName);
        ArrayList<FavColour> favColours = new ArrayList<>();
        for (String eyeconStr: eyeconStrArray) {
            if (!eyeconStr.equals("")) {
                FavColour favColour = FavColour.fromString(eyeconStr);
                favColours.add(favColour);
            }
        }
        return favColours;
    }
}
