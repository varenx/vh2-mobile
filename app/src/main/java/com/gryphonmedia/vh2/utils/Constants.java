package com.gryphonmedia.vh2.utils;

public abstract class Constants {
    public static final String BROWSER = "VH Android App v2.0 by Varenx";
    public static final String APK_URL = "http://ec2-34-253-211-110.eu-west-1.compute.amazonaws.com/android/vh2.apk";
    public static final String DEFAULT_AVATAR = "/img/sys/default_avatar_smile.png";
}
