package com.gryphonmedia.vh2.utils;

import android.text.Html;
import android.text.Spanned;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringProcessor {
    public static Spanned processHtml(String input) {
        String result = input.replace("&apos;", "'");
        result = result.replace("&quot;", "\"");
        result = result.replace("&amp;", "&");
        return Html.fromHtml(result);
    }

    public static boolean containsInsensitive(String haystack, String needle) {
        return Pattern.compile(Pattern.quote(needle), Pattern.CASE_INSENSITIVE).matcher(haystack).find();
    }

    public static String stripLinks(String input) {
        String res = input;
        Pattern regex = Pattern.compile("<a.*?href=\"(.*?)\".*?>(.*?)</a>");
        Matcher matcher = regex.matcher(res);
        while (matcher.find()) {
            String fullLink = matcher.group(0);
            String goodLink = matcher.group(1);
            res = res.replace(fullLink, goodLink);
        }
        return res;
    }

    public static String removeHtml(String input) {
        String res = input;
        Pattern regex = Pattern.compile("<(.).*?>(.*?)</.*?>");
        Matcher matcher = regex.matcher(res);
        while (matcher.find()) {
            String tag = matcher.group(1);
            if ((!tag.equals("b")) && (!tag.equals("strong")) &&
                    (!tag.equals("i")) && (!tag.equals("em")) &&
                    (!tag.equals("u"))) {
                // We only replace the html if it's not a formatter
                res = res.replace(matcher.group(0), matcher.group(2));
            }
        }
        return res;
    }
}
