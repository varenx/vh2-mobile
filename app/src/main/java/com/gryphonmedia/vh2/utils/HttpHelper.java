package com.gryphonmedia.vh2.utils;

import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.franmontiel.persistentcookiejar.ClearableCookieJar;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;

public class HttpHelper {

    private static final String TAG = HttpHelper.class.getSimpleName();

    static OkHttpClient client() {
        return new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();
    }

    public static OkHttpClient clientWithCookieJar(Context context) {
        ClearableCookieJar cookieJar = new PersistentCookieJar(
                new SetCookieCache(),
                new SharedPrefsCookiePersistor(context)
        );
        return new OkHttpClient.Builder().cookieJar(cookieJar).build();
    }

    static RequestBody formBody(String csrf, @Nullable HashMap<String, String> params) {
        FormBody.Builder builder = new FormBody.Builder();
        builder.add("csrf", csrf);
        Log.i(TAG, "Adding CSRF " + csrf);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                builder.add(entry.getKey(), entry.getValue());
            }
        }
        return builder.build();
    }
}
