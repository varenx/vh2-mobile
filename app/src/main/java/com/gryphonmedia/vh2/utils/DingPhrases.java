package com.gryphonmedia.vh2.utils;

import android.content.Context;

public abstract class DingPhrases extends DynamicLists {
    private static final String listName = "dingphrases";

    public static void highlightPhrase(Context context, String phrase) {
        putOnList(context, listName, DynamicLists.getKey(context, phrase));
    }

    public static String[] getHighlightedPhrases(Context context) {
        return getListArray(context, listName);
    }
}
