package com.gryphonmedia.vh2.utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.gryphonmedia.vh2.App;
import com.gryphonmedia.vh2.ui.Notifications;

import java.io.IOException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Ajax {

    private static final String TAG = Ajax.class.getSimpleName();

    private Context mContext;
    private String chatId;
    private String csrf;
    private String cookies;
    private OkHttpClient client;

    public Ajax(Context context, String chatId, String csrf, String cookies) {
        this.mContext = context;
        this.chatId = chatId;
        this.csrf = csrf;
        this.cookies = cookies;
        client = HttpHelper.client();
    }

    public String r(String resource, @Nullable HashMap<String, String> params, @Nullable String method) {
        String responseText = "";
        if (chatId.equals("") || (chatId == null)) {
            Toast.makeText(mContext, "WARNING! chatId not found, likely result is logging you out!", Toast.LENGTH_LONG);
            Notifications.showNotification(
                    mContext,
                    App.CHANNEL_CHAT,
                    "VH2",
                    "chatId not found ERROR!",
                    null,
                    null,
                    null,
                    false
            );
        }
        String url = (resource.startsWith("http")) ? resource : "https://vh.mucktopia.com/u/" + chatId + "/" + resource;

        method = (method == null) ? "GET" : method;

        Request.Builder requestBuilder = new Request.Builder()
                .url(url)
                .header("Referer", "https://vh.mucktopia.com/u/" + chatId + "/jchat.srv")
                .header("Content-Type", "application/x-www-form-urlencoded; charset=UTF8")
                .header("Origin", "https://vh.mucktopia.com")
                .header("User-Agent", Constants.BROWSER)
                .header("X-Requested-With", "XMLHttpRequest")
                .header("Cookie", cookies);
        if (method.equals("post")) {
            requestBuilder.post(HttpHelper.formBody(csrf, params));
        }
        Request request = requestBuilder.build();
        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                if (response.body() != null) {
                    return Objects.requireNonNull(response.body()).string();
                }
                return "";
            }
            responseText = Objects.requireNonNull(response.body()).string();
        } catch (SocketTimeoutException ignored) {
        } catch (IOException e) {
            e.printStackTrace();
        }
        return responseText;
    }

    public String fetchTempUserlist() {
        return r("userlist.srv", null, "get");
    }

    public String defaultAjaxAction(String action, @Nullable HashMap<String, String> params) {
        Log.i(TAG, "Calling default action \"" + action + "\"");
        // Add the action and csrf to params?
        HashMap<String, String> newParams = new HashMap<>();
        newParams.put("action", action);
        if (params != null) {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                newParams.put(entry.getKey(), entry.getValue());
            }
        }
        return r("action.srv", newParams, "post");
    }
}
