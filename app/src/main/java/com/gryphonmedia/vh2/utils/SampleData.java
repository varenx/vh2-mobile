package com.gryphonmedia.vh2.utils;

import com.gryphonmedia.vh2.database.Character;
import com.gryphonmedia.vh2.database.ChatRoom;
import com.gryphonmedia.vh2.database.Post;

import java.util.ArrayList;
import java.util.List;

public class SampleData {
    public static List<ChatRoom> getRooms()
    {
        List<ChatRoom> rooms = new ArrayList<>();
        rooms.add(new ChatRoom(1, "Lounge"));
        rooms.add(new ChatRoom(2, "Game ChatRoom"));
        return rooms;
    }

    public static List<Character> getCharacters()
    {
        List<Character> characters = new ArrayList<>();
        characters.add(new Character(1, "Test_Character"));
        characters.add(new Character(2, "Test_Character"));
        characters.add(new Character(3, "Test_Character"));
        characters.add(new Character(4, "Test_Character"));
        characters.add(new Character(5, "Test_Character"));
        characters.add(new Character(6, "Test_Character"));
        return characters;
    }

    public static List<Post> getPosts()
    {
        return new ArrayList<>();
    }
}
