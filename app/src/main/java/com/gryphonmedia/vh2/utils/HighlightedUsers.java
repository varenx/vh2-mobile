package com.gryphonmedia.vh2.utils;

import android.content.Context;

public abstract class HighlightedUsers extends DynamicLists {

    private static final String listName = "highlights";

    public static boolean isNameHighlighted(Context context, String name) {
        return isOnList(context, listName, DynamicLists.getKey(context, name));
    }

    public static void highlightName(Context context, String name) {
        putOnList(context, listName, DynamicLists.getKey(context, name));
    }

    public static void unhighlightName(Context context, String name) {
        removeFromList(context, listName, DynamicLists.getKey(context, name));
    }

}
