package com.gryphonmedia.vh2.utils;

import android.content.Context;

public abstract class IgnoredUsers extends DynamicLists {

    private static final String listName = "ignores";

    public static boolean isUserIgnored(Context context, String name) {
        return isOnList(context, listName, DynamicLists.getKey(context, name));
    }

    public static void ignoreUser(Context context, String name) {
        putOnList(context, listName, DynamicLists.getKey(context, name));
    }

    public static void unignoreUser(Context context, String name) {
        removeFromList(context, listName, DynamicLists.getKey(context, name));
    }

}
