package com.gryphonmedia.vh2.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.ArrayList;
import java.util.Arrays;

abstract class DynamicLists {
    static String getKey(Context context, String string) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String charName = prefs.getString("current_character", "");
        return charName + ":::" + string;
    }

    static boolean isOnList(Context context, String listName, String string) {
        // For the moment we use shared preferences for storing highlights and ignores
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String listStr = prefs.getString(listName, "");
        String[] listArray = new String[0];
        if (listStr != null) {
            listArray = listStr.split("\\*");
        }
        ArrayList<String> listArrayList = new ArrayList<>(Arrays.asList(listArray));
        return listArrayList.contains(string.toLowerCase());
    }

    static void putOnList(Context context, String listName, String string) {
        // For the moment we use shared preferences for storing highlights and ignores
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String listStr = prefs.getString(listName, "");
        String[] listArray = new String[0];
        if (listStr != null) {
            listArray = listStr.split("\\*");
        }
        ArrayList<String> listArrayList = new ArrayList<>(Arrays.asList(listArray));
        if (!listArrayList.contains(string.toLowerCase())) {
            listArrayList.add(string.toLowerCase());
        }
        String[] listArrayUpdated = listArrayList.toArray(new String[0]);
        String newListStr = TextUtils.join("*", listArrayUpdated);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(listName, newListStr);
        editor.apply();
    }

    static void removeFromList(Context context, String listName, String string) {
        // For the moment we use shared preferences for storing highlights and ignores
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String listStr = prefs.getString(listName, "");
        String[] listArray = new String[0];
        if (listStr != null) {
            listArray = listStr.split("\\*");
        }
        ArrayList<String> listArrayList = new ArrayList<>(Arrays.asList(listArray));
        listArrayList.remove(string.toLowerCase());
        String[] listArrayUpdated = listArrayList.toArray(new String[0]);
        String newListStr = TextUtils.join("*", listArrayUpdated);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(listName, newListStr);
        editor.apply();
    }

    static String[] getListArray(Context context, String listName) {
        // For the moment we use shared preferences for storing highlights and ignores
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        String listStr = prefs.getString(listName, "");
        if (listStr != null) {
            return listStr.split("\\*");
        }
        return new String[0];
    }
}
