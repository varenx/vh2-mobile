package com.gryphonmedia.vh2;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.gryphonmedia.vh2.database.User;
import com.gryphonmedia.vh2.fragments.CharacterPickerFragment;
import com.gryphonmedia.vh2.fragments.LoginFragment;
import com.gryphonmedia.vh2.fragments.RoomFragment;
import com.gryphonmedia.vh2.fragments.dialog.AboutDialogFragment;
import com.gryphonmedia.vh2.fragments.dialog.EyeconDialogFragment;
import com.gryphonmedia.vh2.fragments.dialog.QuickHelpDialogFragment;
import com.gryphonmedia.vh2.fragments.dialog.StatusDialogFragment;
import com.gryphonmedia.vh2.fragments.dialog.UserMenuFragment;
import com.gryphonmedia.vh2.listeners.EyeconListener;
import com.gryphonmedia.vh2.listeners.LoginListener;
import com.gryphonmedia.vh2.listeners.PostMessageListener;
import com.gryphonmedia.vh2.listeners.StatusListener;
import com.gryphonmedia.vh2.models.FavColour;
import com.gryphonmedia.vh2.services.CharacterLoginService;
import com.gryphonmedia.vh2.services.ChatService;
import com.gryphonmedia.vh2.services.LoginService;
import com.gryphonmedia.vh2.services.LogoutService;
import com.gryphonmedia.vh2.services.PostMessageService;
import com.gryphonmedia.vh2.services.SetStatusService;
import com.gryphonmedia.vh2.ui.Notifications;
import com.gryphonmedia.vh2.ui.OnlineAdapter;
import com.gryphonmedia.vh2.viewmodel.MainActivityViewModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity implements LoginListener,
        PostMessageListener, StatusListener, OnlineAdapter.OnItemLongClickListener,
        View.OnClickListener, NavigationView.OnNavigationItemSelectedListener,
        EyeconListener {

    private static final String TAG = "MainActivity";
    private static final int CURRENT_FRAGMENT_LOGIN = 1;
    private static final int CURRENT_FRAGMENT_CHARSELECT = 2;
    private static final int CURRENT_FRAGMENT_ROOM = 3;
    private DrawerLayout drawer;
    private RecyclerView online_list;
    private OnlineAdapter mUsersAdapter;
    private List<User> mUsers = new ArrayList<>();
    public Fragment currentFragment;
    private FloatingActionButton fab;
    private int fragmentId = MainActivity.CURRENT_FRAGMENT_LOGIN;
    private boolean bPaused = false;
    private boolean bHiddenFragmentChange = false;

    private BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            switch (Objects.requireNonNull(intent.getAction())) {
                case LoginService.MY_SERVICE_MESSAGE:
                    displayCharacterSelect();
                    break;
                case LoginService.MY_SERVICE_MESSAGE_FAIL:
                    displayLoginFailure();
                    break;
                case CharacterLoginService.MY_SERVICE_MESSAGE:
                    // Start the chat service
                    startChatService();
                    displayChatRoom();
                    break;
                case LogoutService.MY_SERVICE_MESSAGE:
                    Log.i(TAG, "Logout successful, time to stop the chat service now");
                    stopChatService();
                    displayCharacterSelect();
                    break;
                case ChatService.CHATSERVICE_LOGGEDOUT_MESSAGE:
                    Log.i(TAG, "We got disconnected from the chat");
                    displayCharacterSelect();
                    break;
            }
        }
    };

    private void startChatService() {
        Intent service = new Intent(this, ChatService.class);
        service.setAction(ChatService.ACTION_START_SERVICE);
        startService(service);
    }

    private void stopChatService() {
        Intent service = new Intent(this, ChatService.class);
        service.setAction(ChatService.ACTION_STOP_SERVICE);
        startService(service);
    }

    private void switchToPolling() {
        Intent service = new Intent(this, ChatService.class);
        service.setAction(ChatService.ACTION_SWITCH_TO_POLL_SERVICE);
        startService(service);
    }

    private void switchToRealTime() {
        Intent service = new Intent(this, ChatService.class);
        service.setAction(ChatService.ACTION_SWITCH_TO_REALTIME_SERVICE);
        startService(service);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_main);

        drawer = findViewById(R.id.drawer_layout);
        lockDrawers();
        fab = findViewById(R.id.scroll_fab);
        fab.setOnClickListener(this);
        //drawer.openDrawer(Gravity.RIGHT);
        online_list = findViewById(R.id.onlineList);
        online_list.setHasFixedSize(true);
        // Set up our menu
        NavigationView nav = findViewById(R.id.nav);
        nav.setNavigationItemSelectedListener(this);
        View header = nav.getHeaderView(0);
        TextView copyright = header.findViewById(R.id.copy_varenx);
        copyright.setText(Html.fromHtml(getString(R.string.copyright)));
        // Register our broadcast receiver
        // Pick our actions
        IntentFilter intentFilter = new IntentFilter(LoginService.MY_SERVICE_MESSAGE);
        intentFilter.addAction(LoginService.MY_SERVICE_MESSAGE_FAIL);
        intentFilter.addAction(CharacterLoginService.MY_SERVICE_MESSAGE);
        intentFilter.addAction(LogoutService.MY_SERVICE_MESSAGE);
        intentFilter.addAction(ChatService.CHATSERVICE_LOGGEDOUT_MESSAGE);
        // Register our receiver
        LocalBroadcastManager.getInstance(getApplicationContext())
                .registerReceiver(mBroadcastReceiver, intentFilter);
        setupViewModel();
    }

    @Override
    public void onBackPressed() {
        // We do nothing when the back button is pressed in the room?
        if (fragmentId != MainActivity.CURRENT_FRAGMENT_ROOM) {
            super.onBackPressed();
        }

    }

    @Override
    public void onPostResume() {
        // Resume
        super.onPostResume();
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_LOGIN) {
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);

            final String username = prefs.getString("username", "");
            final String password = prefs.getString("password", "");

            // If we do we will run the login sequence with those credentials, fetch the characters
            if ((username != null) && (password != null)) {
                if (username.equals("") || password.equals("")) {
                    displayLogin();
                } else {
                    doLogin(username, password);
                }
            } else {
                displayLogin();
            }
        }
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_ROOM) {
            displayChatRoom();
        }
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_CHARSELECT) {
            displayCharacterSelect();
        }
    }

    private void setupViewModel() {
        final Observer<List<User>> usersObserver = new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                mUsers.clear();
                mUsers.addAll(users);
                if (mUsersAdapter == null) {
                    mUsersAdapter = new OnlineAdapter(mUsers, MainActivity.this);
                    online_list.setAdapter(mUsersAdapter);
                } else {
                    mUsersAdapter.notifyDataSetChanged();
                }
            }
        };
        MainActivityViewModel vm = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        vm.mUsers.observe(this, usersObserver);
    }

    @SuppressLint("MissingSuperCall")
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        // We ensure we do nothing?
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        LocalBroadcastManager.getInstance(getApplicationContext())
                .unregisterReceiver(mBroadcastReceiver);
    }

    private void showFragment(Fragment fragment) {
        currentFragment = fragment;
        if (!bPaused) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
        } else {
            bHiddenFragmentChange = true;
        }
    }

    @SuppressLint("RestrictedApi")
    private void displayLogin() {
        fragmentId = MainActivity.CURRENT_FRAGMENT_LOGIN;
        LoginFragment loginFragment = new LoginFragment();
        showFragment(loginFragment);
        fab.setVisibility(View.GONE);
        lockDrawers();
    }

    @SuppressLint("RestrictedApi")
    private void displayLoginFailure() {
        // Create a snackbar, set the button back
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_LOGIN) {
            if (currentFragment != null) {
                LoginFragment loginFragment = ((LoginFragment) currentFragment);
                if (loginFragment.isAdded()) {
                    Snackbar.make(
                            findViewById(R.id.drawer_layout),
                            getString(R.string.credential_error),
                            Snackbar.LENGTH_SHORT
                    ).show();
                    loginFragment.enableLoginButton();
                }
            }
        }
        fab.setVisibility(View.GONE);
        lockDrawers();
    }

    @SuppressLint("RestrictedApi")
    private void displayCharacterSelect()
    {
        // Hide keyboard
        InputMethodManager imm = (InputMethodManager) getSystemService(
                Context.INPUT_METHOD_SERVICE
        );
        imm.hideSoftInputFromWindow(drawer.getWindowToken(), 0);
        fragmentId = MainActivity.CURRENT_FRAGMENT_CHARSELECT;
        CharacterPickerFragment characterPickerFragment = new CharacterPickerFragment();
        showFragment(characterPickerFragment);
        fab.setVisibility(View.GONE);
        lockDrawers();
    }

    @SuppressLint("RestrictedApi")
    private void displayChatRoom()
    {
        fragmentId = MainActivity.CURRENT_FRAGMENT_ROOM;
        RoomFragment roomFragment = new RoomFragment();
        showFragment(roomFragment);
        unlockDrawers();
    }

    private void doLogin(String username, String password)
    {
        // Call LoginService here
        Intent intent = new Intent(this, LoginService.class);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        startService(intent);
    }

    @Override
    public void onLogin(String username, String password) {
        doLogin(username, password);
    }

    @Override
    public void onCharacterLogin(int roomId, String charName) {
        Log.i(TAG, "Character selected, time to log in now as " + charName);
        Intent intent = new Intent(this, CharacterLoginService.class);
        intent.putExtra("roomId", roomId);
        intent.putExtra("charName", charName);
        startService(intent);
    }

    @Override
    public void onPostMessage(String postMessage, String msgTarget, String eyeconId) {
        Intent intent = new Intent(this, PostMessageService.class);
        intent.putExtra("postMessage", postMessage);
        intent.putExtra("msgTarget", msgTarget);
        intent.putExtra("eyeconId", Integer.valueOf(eyeconId));
        startService(intent);
    }

    public void onItemLongClicked(int position) {
        String name = mUsersAdapter.getItem(position).getName();
        // Let's create the dialog, if it exists, drop it
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle args = new Bundle();
        args.putString("userName", name);
        UserMenuFragment dialog = new UserMenuFragment();
        dialog.setArguments(args);
        closeDrawer();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    public void closeDrawer() {
        drawer.closeDrawers();
    }

    private void lockDrawers() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    private void unlockDrawers() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.scroll_fab) {
            if (fragmentId == MainActivity.CURRENT_FRAGMENT_ROOM) {
                if (currentFragment != null) {
                    // Check if we have a room fragment
                    ((RoomFragment) currentFragment).setKeepScrolling(true);
                }
            }
        }
    }

    private void showSetStatusDialog() {
        // Let's create the dialog, if it exists, drop it
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle args = new Bundle();
        StatusDialogFragment dialog = new StatusDialogFragment();
        dialog.setArguments(args);
        closeDrawer();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    public FloatingActionButton getFab() {
        return fab;
    }

    @Override
    public void onStatusChange(String code) {
        Log.i(TAG, "Status change to " + code);
        Intent intent = new Intent(this, SetStatusService.class);
        intent.putExtra("status", code);
        startService(intent);
    }

    private void showAboutDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle args = new Bundle();
        AboutDialogFragment dialog = new AboutDialogFragment();
        dialog.setArguments(args);
        closeDrawer();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    public void showHelpDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle args = new Bundle();
        QuickHelpDialogFragment dialog = new QuickHelpDialogFragment();
        dialog.setArguments(args);
        closeDrawer();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    public void showEyeconDialog() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment prev = getSupportFragmentManager().findFragmentByTag("dialog");
        if (prev != null) {
            ft.remove(prev);
        }
        ft.addToBackStack(null);
        Bundle args = new Bundle();
        EyeconDialogFragment dialog = new EyeconDialogFragment();
        dialog.setArguments(args);
        closeDrawer();
        dialog.show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        if (fragmentId != MainActivity.CURRENT_FRAGMENT_ROOM) {
            return false;
        }
        if (menuItem.getItemId() == R.id.nav_set_status) {
            // Display the change status window
            showSetStatusDialog();
            return true;
        }
        if (menuItem.getItemId() == R.id.nav_quick_help) {
            showHelpDialog();
            return true;
        }
        if (menuItem.getItemId() == R.id.nav_app_info) {
            showAboutDialog();
            return true;
        }
        return false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        bPaused = true;
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_ROOM) {
            switchToPolling();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Notifications.clearNotifications(this);
        bPaused = false;
        if (bHiddenFragmentChange) {
            showFragment(currentFragment);
            bHiddenFragmentChange = false;
        }
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_ROOM) {
            switchToRealTime();
        }
    }

    public OnlineAdapter getUsersAdapter() {
        return mUsersAdapter;
    }

    @Override
    public void onEyeconChange(FavColour eyecon) {
        if (fragmentId == MainActivity.CURRENT_FRAGMENT_ROOM) {
            ((RoomFragment) currentFragment).setEyecon(eyecon);
        }
    }
}
