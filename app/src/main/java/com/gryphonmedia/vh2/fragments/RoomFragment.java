package com.gryphonmedia.vh2.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gryphonmedia.vh2.MainActivity;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.Post;
import com.gryphonmedia.vh2.listeners.PostMessageListener;
import com.gryphonmedia.vh2.models.FavColour;
import com.gryphonmedia.vh2.ui.PostsAdapter;
import com.gryphonmedia.vh2.utils.Eyecons;
import com.gryphonmedia.vh2.viewmodel.RoomViewModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class RoomFragment extends Fragment implements View.OnClickListener, PostsAdapter.OnItemLongClickListener, View.OnScrollChangeListener {

    private static final String TAG = "RoomFragment";
    private List<Post> mPosts = new ArrayList<>();
    private PostsAdapter mPostsAdapter;
    private RecyclerView post_list;
    private EditText etMessage;
    private EditText etMessageTo;
    private EditText etEyeconId;
    private TextView msg_targetInfo;
    private ImageView eyeconSelectorImg;
    private View eyeconSelectorColour;
    private PostMessageListener listener;
    private Context mContext;
    private boolean bKeepScrolling = true;

    public RoomFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            listener = (PostMessageListener) context;
        } catch (ClassCastException e) {
            // Do what now?
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_room, container, false);
        Button btnPost = view.findViewById(R.id.post_button);
        btnPost.setOnClickListener(this);
        etMessage = view.findViewById(R.id.post_text);
        etMessageTo = view.findViewById(R.id.message_to);
        etEyeconId = view.findViewById(R.id.eyecon_id);
        msg_targetInfo = view.findViewById(R.id.msg_targetInfo);
        msg_targetInfo.setOnClickListener(this);
        View eyeconSelector = view.findViewById(R.id.eyecon);
        eyeconSelector.setOnClickListener(this);
        // Set the initial eyecon to the first
        eyeconSelectorImg = eyeconSelector.findViewById(R.id.eyecon_img);
        eyeconSelectorColour = eyeconSelector.findViewById(R.id.eyecon_colour);
        // Initial values
        FavColour[] eyecons = Eyecons.getEyecons(getContext());
        if (eyecons[0] != null) {
            String eyeconURL = "https://vh.mucktopia.com/img/eyecon/" + eyecons[0].getEyecon();
            Picasso.get().load(eyeconURL).into(eyeconSelectorImg);
            eyeconSelectorColour.setBackgroundColor(Color.parseColor(eyecons[0].getColour()));
            etEyeconId.setText(Integer.toString(eyecons[0].getId()));
        }
        // And onwards
        post_list = view.findViewById(R.id.chatroom_posts);
        post_list.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        post_list.setLayoutManager(mLayoutManager);
        post_list.setOnScrollChangeListener(this);
        post_list.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View view, int left, int top, int right, final int bottom, int oldLeft, int oldTop, int oldRight, final int oldBottom) {
                post_list.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if ((oldBottom - bottom < 50) && (bKeepScrolling)) {
                            try {
                                post_list.smoothScrollToPosition(mPostsAdapter.getItemCount() - 1);
                            } catch (IllegalArgumentException e) {}
                        } else {
                            post_list.scrollBy(0, oldBottom - bottom);
                        }
                    }
                }, 100);
            }
        });
        setupViewModel();
        // Load up some settings from the saved preferences, such as we're currently whispering
        // someone, that should go in there
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (prefs != null) {
            String whisperTarget = prefs.getString("whisper_target", "");
            if (whisperTarget == null) {
                whisperTarget = "";
            }
            setPrivateTarget(whisperTarget);
            Log.i(TAG, "Loading current eyecon");
            if (eyecons[0] != null) {
                String currentEyecon = prefs.getString("current_eyecon", eyecons[0].toString());
                Log.i(TAG, "Current eyecon is : " + currentEyecon);
                if (currentEyecon == null) {
                    currentEyecon = "";
                }
                if (!currentEyecon.equals("")) {
                    setEyecon(FavColour.fromString(currentEyecon));
                }
            } else {
                Log.i(TAG, "Eyecons list is null...?");
                Log.i(TAG, eyecons.toString());
            }
        }
        return view;
    }

    private void setupViewModel() {
        final Observer<List<Post>> postsObserver = new Observer<List<Post>>() {
            @Override
            public void onChanged(List<Post> posts) {
                mPosts.clear();
                mPosts.addAll(posts);
                if (mPostsAdapter == null) {
                    mPostsAdapter = new PostsAdapter(mPosts, RoomFragment.this);
                    post_list.setAdapter(mPostsAdapter);
                    if (bKeepScrolling) {
                        post_list.scrollToPosition(mPostsAdapter.getItemCount() - 1);
                    }
                } else {
                    mPostsAdapter.notifyDataSetChanged();
                    if (bKeepScrolling) {
                        post_list.smoothScrollToPosition(mPostsAdapter.getItemCount() - 1);
                    }
                }
            }
        };

        RoomViewModel vm = ViewModelProviders.of(this).get(RoomViewModel.class);
        vm.getPosts().observe(this, postsObserver);
    }

    private void sendMessage() {
        String postText = etMessage.getText().toString();
        String messageTo = etMessageTo.getText().toString();
        String eyeconId = etEyeconId.getText().toString();
        Log.i(TAG, "Post message");
        etMessage.setText("");
        listener.onPostMessage(postText, messageTo, eyeconId);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.post_button) {
            // Send the message
            sendMessage();
        }
        if (view.getId() == R.id.msg_targetInfo) {
            setPrivateTarget(""); // Clear the whisper target
        }
        if (view.getId() == R.id.eyecon) {
            // Eyecon selection comes here
            ((MainActivity) mContext).showEyeconDialog();
        }
    }

    private void loadPost() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        String currentPost = prefs.getString("current_post", "");
        etMessage.setText(currentPost);
        Log.i(TAG, "Loaded post: " + currentPost);
    }

    private void savePost() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        String currentPost = etMessage.getText().toString();
        Log.i(TAG, "Saving post: " + currentPost);
        editor.putString("current_post", currentPost);
        editor.apply();
    }

    @Override
    public void onResume() {
        Log.i(TAG, "Resumed fragment");
        if (bKeepScrolling && (mPostsAdapter != null) && (post_list != null)) {
            post_list.scrollToPosition(mPostsAdapter.getItemCount() - 1);
        }
        // If we had a post in the process we really should reload that
        loadPost();
        super.onResume();
    }

    @Override
    public void onPause() {
        savePost();
        super.onPause();
    }

    @SuppressLint("SetTextI18n")
    public void setPrivateTarget(String name) {
        if (name.equals("")) {
            msg_targetInfo.setText("");
            msg_targetInfo.setVisibility(View.GONE);
        } else {
            msg_targetInfo.setText("Private message to " + name + " (tap here to cancel)");
            msg_targetInfo.setVisibility(View.VISIBLE);
        }
        etMessageTo.setText(name);
        // Save this in our preferences
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("whisper_target", name);
        editor.apply();
    }

    @SuppressLint("SetTextI18n")
    public void setEyecon(FavColour eyecon) {
        if (eyecon != null) {
            String eyeconURL = "https://vh.mucktopia.com/img/eyecon/" + eyecon.getEyecon();
            Picasso.get().load(eyeconURL).into(eyeconSelectorImg);
            eyeconSelectorColour.setBackgroundColor(Color.parseColor(eyecon.getColour()));
            etEyeconId.setText(Integer.toString(eyecon.getId()));
            SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
            SharedPreferences.Editor editor = prefs.edit();
            editor.putString("current_eyecon", eyecon.toString());
            editor.apply();
        }
    }

    public void onItemLongClicked(int position) {
        Post post = mPostsAdapter.getItem(position);
        setPrivateTarget(post.getSender());
    }

    @SuppressLint("RestrictedApi")
    public void setKeepScrolling(boolean keepScrolling) {
        bKeepScrolling = keepScrolling;
        if (bKeepScrolling && (mPostsAdapter != null) && (post_list != null)) {
            ((MainActivity) mContext).getFab().setVisibility(View.GONE);
            post_list.smoothScrollToPosition(mPostsAdapter.getItemCount() - 1);
        }
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onScrollChange(View view, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        if (view.getId() == R.id.chatroom_posts) {
            if (!view.canScrollVertically(1) && (scrollY - oldScrollY <= 0)) {
                bKeepScrolling = true;
                ((MainActivity) mContext).getFab().setVisibility(View.GONE);
            } else {
                bKeepScrolling = false;
                ((MainActivity) mContext).getFab().setVisibility(View.VISIBLE);
            }
        }
    }


}
