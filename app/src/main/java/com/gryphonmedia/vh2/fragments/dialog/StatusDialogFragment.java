package com.gryphonmedia.vh2.fragments.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.listeners.StatusListener;
import com.gryphonmedia.vh2.models.Status;
import com.gryphonmedia.vh2.ui.StatusAdapter;

import java.util.ArrayList;
import java.util.List;

public class StatusDialogFragment extends DialogFragment implements View.OnClickListener {
    private static final String TAG = StatusDialogFragment.class.getSimpleName();
    private Spinner spinnerStatus;
    private StatusAdapter mStatusAdapter;
    private List<Status> mStatuses = new ArrayList<>();
    private StatusListener listener;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            listener = (StatusListener) context;
        } catch (ClassCastException e) {
            // Do what now?
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String currentStatus = prefs.getString("current_status", "online");
        View v = inflater.inflate(R.layout.dlg_statuschange, container, false);
        spinnerStatus = v.findViewById(R.id.spinner_status);
        Button btnSetStatus = v.findViewById(R.id.btn_setstatus);
        btnSetStatus.setOnClickListener(this);
        // Set the ... thing. Model
        mStatuses = Status.getStatuses();
        if (mStatusAdapter == null) {
            mStatusAdapter = new StatusAdapter(mContext, android.R.layout.simple_spinner_item, mStatuses);
            spinnerStatus.setAdapter(mStatusAdapter);
            spinnerStatus.setSelection(mStatusAdapter.getPositionByCode(currentStatus));
        }
        Log.i(TAG, "Inflated status dialog view");
        return v;
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_setstatus) {
            Log.i(TAG, "Clicked set status button");
            // Set the status to the selected value
            String newStatus = mStatuses.get(spinnerStatus.getSelectedItemPosition()).getCode();
            listener.onStatusChange(newStatus);
            this.dismiss();
        }
    }
}
