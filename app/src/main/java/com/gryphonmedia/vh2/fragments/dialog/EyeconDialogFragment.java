package com.gryphonmedia.vh2.fragments.dialog;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.listeners.EyeconListener;
import com.gryphonmedia.vh2.models.FavColour;
import com.gryphonmedia.vh2.ui.EyeconsAdapter;
import com.gryphonmedia.vh2.utils.Eyecons;

import java.util.ArrayList;

public class EyeconDialogFragment extends DialogFragment implements View.OnClickListener {

    private static final String TAG = EyeconDialogFragment.class.getSimpleName();

    private Spinner spinnerEyecon;
    private EyeconListener listener;
    private EyeconsAdapter mEyeconsAdapter;
    private ArrayList<FavColour> mEyecons;
    private Context mContext;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            listener = (EyeconListener) context;
        } catch (ClassCastException e) {
            // Do what now?
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        String currentEyecon = prefs.getString("current_eyecon", "");
        View v = inflater.inflate(R.layout.dlg_eyeconchange, container, false);
        spinnerEyecon = v.findViewById(R.id.spinner_eyecon);
        mEyecons = Eyecons.getEyeconList(getContext());
        if (mEyeconsAdapter == null) {
            mEyeconsAdapter = new EyeconsAdapter(mContext, R.layout.eyecons_list_item, mEyecons);
            spinnerEyecon.setAdapter(mEyeconsAdapter);
            Log.i(TAG, "Trying to look up eyecon " + currentEyecon);
            Log.i(TAG, "Position: " + mEyeconsAdapter.getPositionByEyeconString(currentEyecon));
            if (mEyeconsAdapter.getPositionByEyeconString(currentEyecon) != -1) {
                spinnerEyecon.setSelection(mEyeconsAdapter.getPositionByEyeconString(currentEyecon));
            }
        }
        Button btnSetEyecon = v.findViewById(R.id.btn_seteyecon);
        btnSetEyecon.setOnClickListener(this);
        return v;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_seteyecon) {
            // Set the eyecon
            FavColour eyecon = mEyecons.get(spinnerEyecon.getSelectedItemPosition());
            listener.onEyeconChange(eyecon);
            this.dismiss();
        }
    }
}
