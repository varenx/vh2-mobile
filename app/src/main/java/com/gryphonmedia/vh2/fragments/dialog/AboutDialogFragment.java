package com.gryphonmedia.vh2.fragments.dialog;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.gryphonmedia.vh2.R;

public class AboutDialogFragment extends DialogFragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dlg_about, container, false);

        TextView contents = v.findViewById(R.id.about_contents);
        Button btnDismiss = v.findViewById(R.id.about_dismiss);
        btnDismiss.setOnClickListener(this);

        contents.setText(Html.fromHtml(contents.getText().toString()));
        return v;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.about_dismiss) {
            this.dismiss();
        }
    }
}
