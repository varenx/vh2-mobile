package com.gryphonmedia.vh2.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.listeners.LoginListener;


public class LoginFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "LoginFragment";
    private EditText etUsername;
    private EditText etPassword;
    private LoginListener listener;
    private Button btnLogin;

    public LoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (LoginListener) context;
        } catch (ClassCastException e) {
            // Do what now?
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_login, container, false);

        etUsername = v.findViewById(R.id.login_username);
        etPassword = v.findViewById(R.id.login_password);
        btnLogin = v.findViewById(R.id.login_btn);
        btnLogin.setOnClickListener(this);
        return v;
    }

    private void doLogin() {
        String username = etUsername.getText().toString();
        String password = etPassword.getText().toString();
        // Send the message to the activity now
        Log.i(TAG, "Logging in as " + username + " / " + password);
        listener.onLogin(username, password);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.login_btn) {
            // Disable the button and the set the text to something nice
            // ... like 'logging in'
            btnLogin.setEnabled(false);
            btnLogin.setText(getString(R.string.logging_in));
            // Send a message to the activity to perform login
            doLogin();
        }
    }

    public void enableLoginButton() {
        btnLogin.setEnabled(true);
        btnLogin.setText(getString(R.string.login_login));
    }
}
