package com.gryphonmedia.vh2.fragments.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.gryphonmedia.vh2.MainActivity;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.fragments.RoomFragment;
import com.gryphonmedia.vh2.utils.HighlightedUsers;
import com.gryphonmedia.vh2.utils.IgnoredUsers;

public class UserMenuFragment extends DialogFragment implements View.OnClickListener {

    private String userName;
    private String currentChar;
    private TextView tvUsername;
    private Button btnViewProfile;
    private Button btnWhisper;
    private Button btnHighlight;
    private Button btnIgnore;
    private Button btnCancel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        currentChar = prefs.getString("current_character", "");
        if (args != null) {
            userName = args.getString("userName", "");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        return super.onCreateDialog(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.dlg_usermenu, container, false);

        tvUsername = v.findViewById(R.id.username);
        tvUsername.setText(userName);
        btnViewProfile = v.findViewById(R.id.btn_viewprofile);
        btnViewProfile.setOnClickListener(this);
        btnWhisper = v.findViewById(R.id.btn_whisper);
        btnWhisper.setOnClickListener(this);
        btnHighlight = v.findViewById(R.id.btn_highlight);
        btnHighlight.setOnClickListener(this);
        btnIgnore = v.findViewById(R.id.btn_ignore);
        btnIgnore.setOnClickListener(this);
        btnCancel = v.findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
        if (HighlightedUsers.isNameHighlighted(getContext(), userName)) {
            btnHighlight.setText(getResources().getString(R.string.unhighlight));
            btnIgnore.setVisibility(View.GONE);
        }
        if (IgnoredUsers.isUserIgnored(getContext(), userName)) {
            btnIgnore.setText(getResources().getString(R.string.unignore));
            btnHighlight.setVisibility(View.GONE);
        }
        if (userName.equals(currentChar)) {
            btnIgnore.setVisibility(View.GONE);
            btnHighlight.setVisibility(View.GONE);
        }

        return v;
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        super.show(manager, tag);
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().getWindow();
    }

    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#99000000")));
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_whisper) {
            // Clicked the whisper button
            MainActivity activity = (MainActivity) getActivity();
            ((RoomFragment)(activity.currentFragment)).setPrivateTarget(userName);
            this.dismiss();
        }
        if (view.getId() == R.id.btn_viewprofile) {
            // View the user's profile
            this.dismiss();
            String url = "https://vh.mucktopia.com/profile/" + userName;
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(url));
            startActivity(i);
        }
        if (view.getId() == R.id.btn_highlight) {
            // Highlight
            MainActivity activity = (MainActivity) getActivity();
            if (HighlightedUsers.isNameHighlighted(getContext(), userName)) {
                HighlightedUsers.unhighlightName(getContext(), userName);
                activity.getUsersAdapter().notifyDataSetChanged();
            } else {
                HighlightedUsers.highlightName(getContext(), userName);
                activity.getUsersAdapter().notifyDataSetChanged();
            }

            this.dismiss();
        }
        if (view.getId() == R.id.btn_ignore) {
            // Ignore
            MainActivity activity = (MainActivity) getActivity();
            if (IgnoredUsers.isUserIgnored(getContext(), userName)) {
                IgnoredUsers.unignoreUser(getContext(), userName);
                activity.getUsersAdapter().notifyDataSetChanged();
            } else {
                IgnoredUsers.ignoreUser(getContext(), userName);
                activity.getUsersAdapter().notifyDataSetChanged();
            }

            this.dismiss();
        }
        if (view.getId() == R.id.btn_cancel) {
            this.dismiss();
        }
    }
}
