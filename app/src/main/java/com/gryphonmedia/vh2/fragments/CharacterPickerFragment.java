package com.gryphonmedia.vh2.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Spinner;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.gryphonmedia.vh2.MainActivity;
import com.gryphonmedia.vh2.R;
import com.gryphonmedia.vh2.database.Character;
import com.gryphonmedia.vh2.database.ChatRoom;
import com.gryphonmedia.vh2.listeners.LoginListener;
import com.gryphonmedia.vh2.ui.CharactersAdapter;
import com.gryphonmedia.vh2.ui.RoomsAdapter;
import com.gryphonmedia.vh2.viewmodel.CharacterPickerViewModel;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CharacterPickerFragment extends Fragment implements View.OnClickListener {

    private final static String TAG = CharacterPickerFragment.class.getSimpleName();

    private List<Character> mCharacters = new ArrayList<>();
    private List<ChatRoom> mRooms = new ArrayList<>();
    private Spinner character_list;
    private Spinner room_list;
    private CharactersAdapter mCharAdapter;
    private RoomsAdapter mRoomAdapter;
    private LoginListener listener;
    private Button btnCharacterLogin;
    private String lastCharacter = "";
    private Context mContext;

    public CharacterPickerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
        try {
            listener = (LoginListener) context;
        } catch (ClassCastException e) {
            // Do what now?
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        lastCharacter = prefs.getString("current_character", "");
        View v = inflater.inflate(R.layout.fragment_character_picker, container, false);
        character_list = v.findViewById(R.id.character);
        if (mCharAdapter == null) {
            mCharAdapter = new CharactersAdapter(mContext, android.R.layout.simple_spinner_item, mCharacters);
            character_list.setAdapter(mCharAdapter);
        }
        // Room list
        room_list = v.findViewById(R.id.chatroom_posts);
        if (mRoomAdapter == null) {
            mRoomAdapter = new RoomsAdapter(mContext, android.R.layout.simple_spinner_item, mRooms);
            room_list.setAdapter(mRoomAdapter);
        }
        // Add the character pick button and its event handler
        btnCharacterLogin = v.findViewById(R.id.btn_character_pick);
        btnCharacterLogin.setOnClickListener(this);

        // Quick help button
        Button btnQuickHelp = v.findViewById(R.id.btn_help);
        btnQuickHelp.setOnClickListener(this);

        setupViewModel();
        return v;
    }

    private void setupViewModel() {
        final Observer<List<Character>> charactersObserver = new Observer<List<Character>>() {
            @Override
            public void onChanged(List<Character> characters) {
                mCharacters.clear();
                mCharacters.addAll(characters);
                if (mCharAdapter == null) {
                    mCharAdapter = new CharactersAdapter(mContext, android.R.layout.simple_spinner_item, mCharacters);
                    character_list.setAdapter(mCharAdapter);
                } else {
                    mCharAdapter.notifyDataSetChanged();
                }
                if (!lastCharacter.equals("")) {
                    character_list.setSelection(mCharAdapter.getPositionByName(lastCharacter));
                }
            }
        };
        final Observer<List<ChatRoom>> roomsObserver = new Observer<List<ChatRoom>>() {
            @Override
            public void onChanged(List<ChatRoom> rooms) {
                mRooms.clear();
                mRooms.addAll(rooms);
                if (mRoomAdapter == null) {
                    mRoomAdapter = new RoomsAdapter(mContext, android.R.layout.simple_spinner_item, mRooms);
                    room_list.setAdapter(mRoomAdapter);
                } else {
                    mRoomAdapter.notifyDataSetChanged();
                }
                room_list.setSelection(mRoomAdapter.getPositionById(2));
            }
        };
        CharacterPickerViewModel vm = ViewModelProviders.of(this).get(CharacterPickerViewModel.class);
        vm.mCharacters.observe(this, charactersObserver);
        vm.mRooms.observe(this, roomsObserver);
    }

    private void doCharacterLogin()
    {
        ChatRoom room = ((ChatRoom)room_list.getSelectedItem());
        Character character = ((Character)character_list.getSelectedItem());
        String roomName = room.getName();
        String characterName = character.getName();
        int roomId = room.getId();
        Log.i(TAG, "Logging in to room " + roomName + "(" + roomId + ") as " + characterName);
        listener.onCharacterLogin(roomId, characterName);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btn_character_pick) {
            btnCharacterLogin.setEnabled(false);
            btnCharacterLogin.setText(getString(R.string.logging_in));
            doCharacterLogin();
        }

        if (view.getId() == R.id.btn_help) {
            ((MainActivity) mContext).showHelpDialog();
        }
    }
}
